<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => "Default User",
            'email' => "user@deffault.com",
            'mobile_number' => "123456789",
            'password' => Hash::make('password')
        ]);
    }
}
