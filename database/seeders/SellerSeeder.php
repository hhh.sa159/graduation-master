<?php

namespace Database\Seeders;

use App\Models\Seller;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class SellerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Seller::create([
            'name' => "Seller Default",
            'email' => "seller@default.com",
            'mobile_number' => "123456789",
            'password' => Hash::make('password')
        ]);
    }
}
