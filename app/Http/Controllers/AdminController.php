<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $date = Admin::all();
        return response()->view('cmsAdmin.admin.index',['admins'=>$date]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return response()->view('cmsAdmin.admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator($request->all(),
        [
        'name'=>'required|string|min:3|max:15',
        'email'=>'required|string|email'],);
        if(!$validator->fails()){
            $admin = new Admin();
            $admin->name = $request->get('name');
            $admin->email = $request->get('email');
            $isSaved = $admin->save();
            return response()->json([
                'message'=> $isSaved ? "Successfully" : "Failed"],
            $isSaved ? Response::HTTP_CREATED:
            Response::HTTP_BAD_REQUEST);
        }else{
            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show(Admin $admin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        //
        return response()->view('cmsAdmin.admin.edit',['admin'=>$admin]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $admin)
    {
        //
        $validator = Validator($request->all(),
        ['name'=>'required|string|min:3|max:15',
         'email'=>'required|string|email'],
        );
        if(!$validator->fails()){
            $admin->name = $request->get('name');
            $admin->email = $request->get('email');
            $isUpdated = $admin->save();
            return response()->json([
                'message'=> $isUpdated ? "Successfully" : "Failed"],
            $isUpdated ? Response::HTTP_CREATED:
            Response::HTTP_BAD_REQUEST);
        }else{
            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admin $admin)
    {
        //
        $isDeleted = $admin->delete();
        if($isDeleted){
            return response()->json(['title'=>'Success!','text'=>'Admin Deleted Successfully','icon'=>'success'
        ],Response::HTTP_OK);
        }
        else{
            return response()->json(['title'=>'Failed','text'=>'Faild to Delete Admin','icon'=>'error'
        ],Response::HTTP_BAD_REQUEST);
        }
    }
}
