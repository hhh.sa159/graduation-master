<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categories = Category::all();
        return response()->view('seller.categories.index', ['categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return response()->view('seller.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request->all());
        // $request->validate([
        //     'name'=>'required|string|min:3|max:15',
        //     'section'=>'required',
        //     'active'=>'required',
        // ]);
        // $category = new Category();
        // // $category->name = $request->name;
        // $category->name = $request->get('name');
        // $category->section = $request->get('section');
        // $category->active = $request->get('active');
        // $isSaved = $category->save();
        // if($isSaved)
        // {
        //     session()->flash('message','Category Created Successfully');
        //     //    return redirect()->back();
        //        return redirect()->route('categories.index');
        // }
        $validator = Validator(
            $request->all(),
            [
                'name_ar' => 'required|string|min:3|max:15',
                'name_en' => 'required|string|min:3|max:15',
                'section' => 'required',
            ],
        );
        if (!$validator->fails()) {
            $category = new Category();
            $category->name_ar = $request->get('name_ar');
            $category->name_en = $request->get('name_en');
            $category->section = $request->get('section');
            $category->save();
            return response()->json(
                [
                    'message' => $category ? "Successfully" : "Failed"
                ],
                $category ? Response::HTTP_CREATED :
                    Response::HTTP_BAD_REQUEST
            );
        } else {
            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
        return response()->view('seller.categories.edit', ['category' => $category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //
        $validator = Validator(
            $request->all(),
            [
                'name' => 'required|string|min:3|max:15',
                'section' => 'required',
                'active' => 'required'
            ],
        );
        if (!$validator->fails()) {
            $category->name = $request->get('name');
            $category->section = $request->get('section');
            $category->active = $request->get('active');
            $isUpdated = $category->save();
            return response()->json(
                [
                    'message' => $isUpdated ? "Successfully" : "Failed"
                ],
                $isUpdated ? Response::HTTP_CREATED :
                    Response::HTTP_BAD_REQUEST
            );
        } else {
            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
        $isDeleted = $category->delete();
        if ($isDeleted) {
            return response()->json([
                'title' => 'Success!', 'text' => 'Category Deleted Successfully', 'icon' => 'success'
            ], Response::HTTP_OK);
        } else {
            return response()->json([
                'title' => 'Failed', 'text' => 'Faild to Delete Category', 'icon' => 'error'
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
