<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    //
    public function showLogin()
    {
        return response()->view('login');
    }
    public function login(Request $request)
    {
        $validator = Validator($request->all(), [
            'mobile_number' => 'required|email|exists:users,mobile_number',
            'password' => 'required|string|min:3|max:30',
        ]);
        if (!$validator->fails()) {
            $credentials = [
                'mobile_number' => $request->get('mobile_number'),
                'password' => $request->get('password'),
            ];
            if (Auth::guard('users')->attempt($credentials)) {
                return redirect('/');
                // return response()->json([
                //     'message' => 'Logged in Successfully'
                // ], Response::HTTP_OK);
            }
            // else {
            //     return response()->json([
            //         'message' => 'Login Failed, wrong credentials'
            //     ], Response::HTTP_BAD_REQUEST);
            // }
        } else {
            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }
    public function logout(Request $request)
    {

        // auth('admin')->logout();
        Auth::guard('users')->logout();
        $request->session()->invalidate();
        return redirect()->route('login');
    }
}
