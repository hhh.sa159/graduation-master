<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    //
    public function showLogin(){
        return response()->view('cmsAdmin.login');
    }
    public function login(Request $request){
        $validator = Validator($request->all(),[
            // 'email' => 'required|email|exists:admins,email',
            'email' => 'required|email|exists:admins,email',
            'password' => 'required|string|min:3|max:30',
            'remember' => 'required|boolean',
            // 'guard' => 'required|string|in:admin,seller',
        ],
        // [
        //     'guard.in' => 'Please , Check Login URL'
        // ]
    );
        if(!$validator->fails()){
            $credentials = [
                'email' => $request->get('email'),
                'password' => $request->get('password'),
            ];
            if(Auth::guard('admin')->attempt($credentials,$request->get('remember'))){
                return response()->json(['message' => 'Logged in Successfully'
            ],Response::HTTP_OK);
            }else{
                return response()->json([
                    'message' => 'Login Failed, wrong credentials'
                ], Response::HTTP_BAD_REQUEST);
            }
        }else{
            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }
    public function logout(Request $request){

        // auth('admin')->logout();
        Auth::guard('admin')->logout();
        $request->session()->invalidate();
        return redirect()->route('login.admin');

        // if(auth('admin')->check()){
        //     auth::guard('admin')->logout();
        //     $request->session()->invalidate();
        //     return redirect()->route('login','admin');
        // }else{
        //     auth::guard('seller')->logout();
        //     $request->session()->invalidate();
        //     return redirect()->route('login','seller');
        // }

        // $guard = auth('admin')->check() ? 'admin ' : 'seller';
        // auth::guard($guard)->logout();
        // $request->session()->invalidate();
        // return redirect()->route('login',$guard);

    }
}
