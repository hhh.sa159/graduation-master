<?php

namespace App\Http\Controllers;

use App\Models\Seller;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SellerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $date = Seller::all();
        return response()->view('cmsAdmin.seller.index',['sellers'=>$date]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return response()->view('cmsAdmin.seller.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator($request->all(),
        [
        'name'=>'required|string|min:3|max:15',
        'email'=>'required|string|email',
        'mobile_number'=>'required'],);
        if(!$validator->fails()){
            $seller = new Seller();
            $seller->name = $request->get('name');
            $seller->email = $request->get('email');
            $seller->mobile_number = $request->get('mobile_number');
            $isSaved = $seller->save();
            return response()->json([
                'message'=> $isSaved ? "Successfully" : "Failed"],
            $isSaved ? Response::HTTP_CREATED:
            Response::HTTP_BAD_REQUEST);
        }else{
            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Seller  $seller
     * @return \Illuminate\Http\Response
     */
    public function show(Seller $seller)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Seller  $seller
     * @return \Illuminate\Http\Response
     */
    public function edit(Seller $seller)
    {
        //
        return response()->view('cmsAdmin.seller.edit',['seller'=>$seller]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Seller  $seller
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Seller $seller)
    {
        //
        $validator = Validator($request->all(),
        ['name'=>'required|string|min:3|max:15',
         'email'=>'required|string|email',
         'mobile_number'=>'required'],
        );
        if(!$validator->fails()){
            $seller->name = $request->get('name');
            $seller->email = $request->get('email');
            $seller->mobile_number = $request->get('mobile_number');
            $isUpdated = $seller->save();
            return response()->json([
                'message'=> $isUpdated ? "Successfully" : "Failed"],
            $isUpdated ? Response::HTTP_CREATED:
            Response::HTTP_BAD_REQUEST);
        }else{
            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Seller  $seller
     * @return \Illuminate\Http\Response
     */
    public function destroy(Seller $seller)
    {
        //
        $isDeleted = $seller->delete();
        if($isDeleted){
            return response()->json(['title'=>'Success!','text'=>'seller Deleted Successfully','icon'=>'success'
        ],Response::HTTP_OK);
        }
        else{
            return response()->json(['title'=>'Failed','text'=>'Faild to Delete seller','icon'=>'error'
        ],Response::HTTP_BAD_REQUEST);
        }
    }
}
