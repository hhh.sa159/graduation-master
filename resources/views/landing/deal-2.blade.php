@extends('landing.parent')

@section('Home-Title','Deal 2')

@section('add-deal')

<div class="landing-notis-head d-flex justify-content-center align-items-center">
    <p class="m-0">هل تريد ان تبدء صفقة جديدة الان ؟ </p><a class="btnn-orange btn btnn hvr-shadow btn-fill" href="{{route('add-deal')}}"><span>اضافة صفقة</span></a>
  </div>

@endsection

@section('main-content')

<div class="main-content pb-5">
    <div class="header">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{route('home')}}">الرئيسية</a></li>
          <li class="breadcrumb-item">الصفقات</li>
        </ol>
      </nav>
    </div>
    <div class="page-content-inner bargains-request mt-5">
      <div class="add-product-title d-flex justify-content-between">
        <h2>تفاصيل العرض</h2>
        <button class="btnn-orange btn btnn hvr-shadow" type="button"><span>جارى التوصيل</span>
        </button>
      </div>
      <div class="order-details">
        <div class="row">
          <div class="col-lg-4">
            <div class="order-details-box"> <span>رقم العرض </span>
              <h5>#6461313</h5>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="order-details-box"> <span> تاريخ ووقت الطلب </span>
              <h5>   12 سبتمبر ، 2021 -  50:4 مساءً</h5>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="order-details-box"> <span>إجمالي الطلب</span>
              <h5> 00.64 شيكل</h5>
            </div>
          </div>
        </div>
      </div>
      <hr class="my-5 cus-hr">
      <div class="add-product-title">
        <h2>تفاصيل التاجر </h2>
        <div class="merchant-details d-flex justify-content-between mb-4">
          <div class="d-flex justify-content-start align-items-center">
            <div class="img me-3"> <img class="img" src="../../assets/images/landing/avatar.png" alt=""/></div>
            <div class="info">
              <h6>التاجر</h6>
              <h5 class="mb-0">Grocery shope</h5>
            </div>
          </div>
          <div class="d-flex justify-content-end align-items-center">
            <div class="message-view-box-actions"> <a class="btnn-orange btn btnn hvr-shadow" href="{{route('chat')}}">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#envelope"></use>
                </svg></a></div>
          </div>
        </div>
      </div>
      <hr class="my-5 cus-hr">
      <div class="add-product-title">
        <h2>اﻟﻤﻨﺘﺠﺎت </h2>
      </div>
      <div class="table-container">
        <div class="dash-table table-responsive">
          <table class="table responsive" id="">
            <thead>
              <th>المنتج</th>
              <th>الإسم</th>
              <th>القسم</th>
              <th>السعر</th>
              <th>الكمية</th>
              <th>الإجراءات</th>
            </thead>
            <tbody>
              <tr>
                <td><div class="table-img"><img src="../../assets/images/product-thumb.png" /></div></td>
                <td><div class="table-two-texts"><h5>خضراوات مشكل</h5><h6>#54545454</h6></div></td>
                <td><div class="table-two-texts"><h5>بقالة</h5><h6> خضراوات </h6></div></td>
                <td><h5 class="textt-green">45 شيكل</h5></td>
                <td><h5><strong>5</strong></h5></td>
                <td>
                  <div class="table-actions">
                    <div class="available-check">
                      <input type="checkbox" id="available-check475"/>
                      <label class="btn hvr-shadow" for="available-check475">متوفر لدي</label>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>
                <td><div class="table-img"><img src="../../assets/images/product-thumb.png" /></div></td>
                <td><div class="table-two-texts"><h5>خضراوات مشكل</h5><h6>#54545454</h6></div></td>
                <td><div class="table-two-texts"><h5>بقالة</h5><h6> خضراوات </h6></div></td>
                <td><h5 class="textt-green">45 شيكل</h5></td>
                <td><h5><strong>5</strong></h5></td>
                <td>
                  <div class="table-actions">
                    <div class="available-check">
                      <input type="checkbox" id="available-check541"/>
                      <label class="btn hvr-shadow" for="available-check541">متوفر لدي</label>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>
                <td><div class="table-img"><img src="../../assets/images/product-thumb.png" /></div></td>
                <td><div class="table-two-texts"><h5>خضراوات مشكل</h5><h6>#54545454</h6></div></td>
                <td><div class="table-two-texts"><h5>بقالة</h5><h6> خضراوات </h6></div></td>
                <td><h5 class="textt-green">45 شيكل</h5></td>
                <td><h5><strong>5</strong></h5></td>
                <td>
                  <div class="table-actions">
                    <div class="available-check">
                      <input type="checkbox" id="available-check186"/>
                      <label class="btn hvr-shadow" for="available-check186">متوفر لدي</label>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>
                <td><div class="table-img"><img src="../../assets/images/product-thumb.png" /></div></td>
                <td><div class="table-two-texts"><h5>خضراوات مشكل</h5><h6>#54545454</h6></div></td>
                <td><div class="table-two-texts"><h5>بقالة</h5><h6> خضراوات </h6></div></td>
                <td><h5 class="textt-green">45 شيكل</h5></td>
                <td><h5><strong>5</strong></h5></td>
                <td>
                  <div class="table-actions">
                    <div class="available-check">
                      <input type="checkbox" id="available-check754"/>
                      <label class="btn hvr-shadow" for="available-check754">متوفر لدي</label>
                    </div>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <hr class="my-5 cus-hr">
      <div class="add-product-title">
        <h2>تفاصيل الشحن</h2>
      </div>
      <div class="row row-cols-1 row-cols-md-2 align-items-end">
        <div class="col">
          <div class="order-details-box"> <span> تاريخ ووقت الطلب </span>
            <h5>   12 سبتمبر ، 2021 -  50:4 مساءً</h5>
          </div>
        </div>
        <div class="col">
          <div class="order-details-box"> <span> خيارات الشحن </span>
            <h5> خيارات الشحن  <b class="textt-red">50 شيكل + 20 شيكل توصيل سريع</b></h5>
          </div>
        </div>
      </div>
      <div class="add-product-title pt-5">
        <h2>ملاجظاتك</h2>
        <p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام "هنا يوجد محتوى نصي، هنا يوجد محتوى نصي" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء        </p>
      </div>
      <hr class="my-5 cus-hr">
      <div class="add-product-title">
        <h2>الفاتورة</h2>
      </div>
      <div class="order-details">
        <div class="order-total">
          <ul class="p-0 m-0">
            <li>
              <h5>السعر الفرعي </h5>
              <h6>350 شيكل</h6>
            </li>
            <li>
              <h5>التوصيل<span class="express">Express </span></h5>
              <h6>350 شيكل</h6>
            </li>
            <li>
              <h5>كوبون الخصم </h5>
              <h6>20 شيكل</h6>
            </li>
            <li>
              <h5>إجمالي الطلبات</h5>
              <h6 class="fw-bold">620 شيكل</h6>
            </li>
          </ul>
          <hr>
        </div>
      </div>
      <div class="d-flex justify-content-between flex-wrap">
        <div class="add-product-title">
          <h2>وﺳﺎﺋﻞ اﻟﺪﻓﻊ اﻟﻤﻨﺎﺳﺒﺔ ﻟﻚ</h2>
        </div>
        <div class="payments-list-check">
          <ul class="p-0 m-0">
            <li>
              <input class="active" type="radio" name="payment" id="visa">
              <label class="active" for="visa">
                <div class="title">فيزا / ماستر كارد</div><img class="img" src="../../assets/images/payments/visa.png" alt=""><img class="img-active" src="../../assets/images/payments/visa-active.png" alt="">
              </label>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>

@endsection

