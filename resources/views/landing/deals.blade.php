@extends('landing.parent')

@section('Home-Title','Deals')

@section('add-deal')

<div class="landing-notis-head d-flex justify-content-center align-items-center">
    <p class="m-0">هل تريد ان تبدء صفقة جديدة الان ؟ </p><a class="btnn-orange btn btnn hvr-shadow btn-fill" href="{{route('add-deal')}}"><span>اضافة صفقة</span></a>
  </div>

@endsection

@section('main-content')

<div class="main-content pb-5">
    <div class="header">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{route('home')}}">الرئيسية</a></li>
          <li class="breadcrumb-item">الصفقات</li>
        </ol>
      </nav>
    </div>
    <div class="row row-cols-md-2 row-cols-lg-4 row-cols-xl-4 match-height mt-5">
      <div class="col"> <a class="cus-bargains hvr-float-shadow" href="{{route('deal-1')}}">
          <div class="top-box"> <span>عدد المنتجات المطلوبة </span><span>منذ ساعتين</span></div>
          <div class="total"> 5 منتجات</div>
          <div class="image"> <img src="../../assets/images/lemons-1.png" alt=""/></div>
          <div class="price-row">
            <div class="price"> <span>  ﺳﻌﺮ التقريبي للمنتجات</span>
              <h3>350 شيكل </h3>
            </div>
          </div>
          <div class="buttons">
            <button class="btnn-orange btn btnn hvr-shadow" type="button"><span>بقالة </span>
            </button>
            <button class="btnn-orange btn btnn hvr-shadow" type="button"><span>خضراوات </span>
            </button>
          </div>
          <div class="progress">
            <div class="progress-bar bg-success" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
          <div class="progress-text">   العروض المقدمة 50 عرض</div></a>
      </div>
      <div class="col"> <a class="cus-bargains hvr-float-shadow" href="{{route('deal-1')}}">
          <div class="top-box"> <span>عدد المنتجات المطلوبة </span><span>منذ ساعتين</span></div>
          <div class="total"> 5 منتجات</div>
          <div class="image"> <img src="../../assets/images/lemons-1.png" alt=""/></div>
          <div class="price-row">
            <div class="price"> <span>  ﺳﻌﺮ التقريبي للمنتجات</span>
              <h3>350 شيكل </h3>
            </div>
          </div>
          <div class="buttons">
            <button class="btnn-orange btn btnn hvr-shadow" type="button"><span>بقالة </span>
            </button>
            <button class="btnn-orange btn btnn hvr-shadow" type="button"><span>خضراوات </span>
            </button>
          </div>
          <div class="progress">
            <div class="progress-bar bg-success" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
          <div class="progress-text">   العروض المقدمة 50 عرض</div></a>
      </div>
      <div class="col"> <a class="cus-bargains hvr-float-shadow" href="{{route('deal-1')}}">
          <div class="top-box"> <span>عدد المنتجات المطلوبة </span><span>منذ ساعتين</span></div>
          <div class="total"> 5 منتجات</div>
          <div class="image"> <img src="../../assets/images/lemons-1.png" alt=""/></div>
          <div class="price-row">
            <div class="price"> <span>  ﺳﻌﺮ التقريبي للمنتجات</span>
              <h3>350 شيكل </h3>
            </div>
          </div>
          <div class="buttons">
            <button class="btnn-orange btn btnn hvr-shadow" type="button"><span>بقالة </span>
            </button>
            <button class="btnn-orange btn btnn hvr-shadow" type="button"><span>خضراوات </span>
            </button>
          </div>
          <div class="progress">
            <div class="progress-bar bg-success" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
          <div class="progress-text">   العروض المقدمة 50 عرض</div></a>
      </div>
      <div class="col"> <a class="cus-bargains hvr-float-shadow" href="{{route('deal-1')}}">
          <div class="top-box"> <span>عدد المنتجات المطلوبة </span><span>منذ ساعتين</span></div>
          <div class="total"> 5 منتجات</div>
          <div class="image"> <img src="../../assets/images/lemons-1.png" alt=""/></div>
          <div class="price-row">
            <div class="price"> <span>  ﺳﻌﺮ التقريبي للمنتجات</span>
              <h3>350 شيكل </h3>
            </div>
          </div>
          <div class="buttons">
            <button class="btnn-orange btn btnn hvr-shadow" type="button"><span>بقالة </span>
            </button>
            <button class="btnn-orange btn btnn hvr-shadow" type="button"><span>خضراوات </span>
            </button>
          </div>
          <div class="progress">
            <div class="progress-bar bg-success" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
          <div class="progress-text">   العروض المقدمة 50 عرض</div></a>
      </div>
      <div class="col"> <a class="cus-bargains hvr-float-shadow" href="{{route('deal-1')}}">
          <div class="top-box"> <span>عدد المنتجات المطلوبة </span><span>منذ ساعتين</span></div>
          <div class="total"> 5 منتجات</div>
          <div class="image"> <img src="../../assets/images/lemons-1.png" alt=""/></div>
          <div class="price-row">
            <div class="price"> <span>  ﺳﻌﺮ التقريبي للمنتجات</span>
              <h3>350 شيكل </h3>
            </div>
          </div>
          <div class="buttons">
            <button class="btnn-orange btn btnn hvr-shadow" type="button"><span>بقالة </span>
            </button>
            <button class="btnn-orange btn btnn hvr-shadow" type="button"><span>خضراوات </span>
            </button>
          </div>
          <div class="progress">
            <div class="progress-bar bg-success" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
          <div class="progress-text">   العروض المقدمة 50 عرض</div></a>
      </div>
      <div class="col"> <a class="cus-bargains hvr-float-shadow" href="{{route('deal-1')}}">
          <div class="top-box"> <span>عدد المنتجات المطلوبة </span><span>منذ ساعتين</span></div>
          <div class="total"> 5 منتجات</div>
          <div class="image"> <img src="../../assets/images/lemons-1.png" alt=""/></div>
          <div class="price-row">
            <div class="price"> <span>  ﺳﻌﺮ التقريبي للمنتجات</span>
              <h3>350 شيكل </h3>
            </div>
          </div>
          <div class="buttons">
            <button class="btnn-orange btn btnn hvr-shadow" type="button"><span>بقالة </span>
            </button>
            <button class="btnn-orange btn btnn hvr-shadow" type="button"><span>خضراوات </span>
            </button>
          </div>
          <div class="progress">
            <div class="progress-bar bg-success" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
          <div class="progress-text">   العروض المقدمة 50 عرض</div></a>
      </div>
      <div class="col"> <a class="cus-bargains hvr-float-shadow" href="{{route('deal-1')}}">
          <div class="top-box"> <span>عدد المنتجات المطلوبة </span><span>منذ ساعتين</span></div>
          <div class="total"> 5 منتجات</div>
          <div class="image"> <img src="../../assets/images/lemons-1.png" alt=""/></div>
          <div class="price-row">
            <div class="price"> <span>  ﺳﻌﺮ التقريبي للمنتجات</span>
              <h3>350 شيكل </h3>
            </div>
          </div>
          <div class="buttons">
            <button class="btnn-orange btn btnn hvr-shadow" type="button"><span>بقالة </span>
            </button>
            <button class="btnn-orange btn btnn hvr-shadow" type="button"><span>خضراوات </span>
            </button>
          </div>
          <div class="progress">
            <div class="progress-bar bg-success" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
          <div class="progress-text">   العروض المقدمة 50 عرض</div></a>
      </div>
      <div class="col"> <a class="cus-bargains hvr-float-shadow" href="{{route('deal-1')}}">
          <div class="top-box"> <span>عدد المنتجات المطلوبة </span><span>منذ ساعتين</span></div>
          <div class="total"> 5 منتجات</div>
          <div class="image"> <img src="../../assets/images/lemons-1.png" alt=""/></div>
          <div class="price-row">
            <div class="price"> <span>  ﺳﻌﺮ التقريبي للمنتجات</span>
              <h3>350 شيكل </h3>
            </div>
          </div>
          <div class="buttons">
            <button class="btnn-orange btn btnn hvr-shadow" type="button"><span>بقالة </span>
            </button>
            <button class="btnn-orange btn btnn hvr-shadow" type="button"><span>خضراوات </span>
            </button>
          </div>
          <div class="progress">
            <div class="progress-bar bg-success" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
          <div class="progress-text">   العروض المقدمة 50 عرض</div></a>
      </div>
    </div>
  </div>

@endsection

