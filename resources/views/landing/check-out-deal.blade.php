@extends('landing.parent')

@section('Home-Title','Check Out Deal')

@section('main-content')

<div class="main-content pb-5">
    <div class="order-success text-center">
      <div class="icon">
        <svg>
          <use href="../../assets/images/icons/icons.svg#check"></use>
        </svg>
      </div>
      <h3 class="mt-5 fw-bold">تم ترسية الصفقة بنجاح</h3><a href="#">التاجر Grocery shope </a>
      <div class="order-total mt-5">
        <ul>
          <li>
            <h5>عدد المنتجات</h5>
            <h6>2</h6>
          </li>
          <li>
            <h5>اجمالى الطلب </h5>
            <h6>350 شيكل</h6>
          </li>
          <li>
            <h5>طريقة الدفع </h5>
            <h6>فيزا</h6>
          </li>
          <li>
            <h5>رقم الطلب</h5>
            <h6>#646431</h6>
          </li>
        </ul>
        <div class="comp-order mt-4"><a class="btnn-orange btn btnn hvr-shadow btn-fill px-5" href="{{route('deals')}}"><span>اتمام الطلب </span></a></div>
      </div>
      <div class="text-center mt-5">
        <h3>لنا مكان فى كل دُ كان</h3>
        <p>جميع عمليات الدفع والطلب لدينا مضمونة 100%     </p>
      </div>
    </div>
  </div>

@endsection

