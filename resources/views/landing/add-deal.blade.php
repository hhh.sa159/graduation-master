@extends('landing.parent')

@section('Home-Title','Add Deal')

@section('add-deal')

<div class="landing-notis-head d-flex justify-content-center align-items-center">
    <p class="m-0">هل تريد ان تبدء صفقة جديدة الان ؟ </p><a class="btnn-orange btn btnn hvr-shadow btn-fill" href="{{route('add-deal')}}"><span>اضافة صفقة</span></a>
  </div>

@endsection

@section('main-content')

<div class="main-content pb-5">
    <div class="header">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{route('home')}}">الرئيسية</a></li>
          <li class="breadcrumb-item">اضافة صفقة</li>
        </ol>
      </nav>
    </div>
    <ul class="nav nav-tabs d-none mt-5">
      <li class="nav-item" role="presentation">
        <button class="nav-link active" id="step-1-details-tab" data-bs-toggle="tab" data-bs-target="#step-1-details" type="button" role="tab" aria-controls="step-1-details" aria-selected="true">Home</button>
      </li>
      <li class="nav-item" role="presentation">
        <button class="nav-link" id="step-2-details-tab" data-bs-toggle="tab" data-bs-target="#step-2-details" type="button" role="tab" aria-controls="step-2-details" aria-selected="false">Profile</button>
      </li>
      <li class="nav-item" role="presentation">
        <button class="nav-link" id="step-3-details-tab" data-bs-toggle="tab" data-bs-target="#step-3-details" type="button" role="tab" aria-controls="step-3-details" aria-selected="false">Messages</button>
      </li>
    </ul>
    <div class="add-product cart-content">
      <div class="tab-content">
        <div class="tab-pane fade show active" id="step-1-details" role="tabpanel" aria-labelledby="step-1-details-tab">
          <div class="add-product-body">
            <div class="add-product-title">
              <h2>صور المنتج </h2>
              <p>يمكنك إضافة صور بحد أقصى 4 صور للمنتج <span class="textt-red mr-2">مقاس 1080*1080</span></p>
            </div>
            <div class="row row-cols-1 mb-5 row-cols-sm-2 row-cols-md-4">
              <div class="col">
                <div class="image-upload">
                  <div class="input-image">
                    <input type="file"/><img class="preview-image" src="" alt=""/>
                    <div class="icon">
                      <svg>
                        <use href="../../assets/images/icons/icons.svg#gallery"></use>
                      </svg>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col">
                <div class="image-upload">
                  <div class="input-image">
                    <input type="file"/><img class="preview-image" src="" alt=""/>
                    <div class="icon">
                      <svg>
                        <use href="../../assets/images/icons/icons.svg#gallery"></use>
                      </svg>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col">
                <div class="image-upload">
                  <div class="input-image">
                    <input type="file"/><img class="preview-image" src="" alt=""/>
                    <div class="icon">
                      <svg>
                        <use href="../../assets/images/icons/icons.svg#gallery"></use>
                      </svg>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col">
                <div class="image-upload">
                  <div class="input-image">
                    <input type="file"/><img class="preview-image" src="" alt=""/>
                    <div class="icon">
                      <svg>
                        <use href="../../assets/images/icons/icons.svg#gallery"></use>
                      </svg>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="add-product-title">
              <h2>وصف المنتج</h2>
            </div>
            <div class="custom-js-tabs">
              <ul class="nav nav-tabs form-lang-tabs" role="tablist" id="tabs-175">
                <li class="nav-item" role="presentation">
                  <button class="nav-link active" data-tab="arabic-tab" type="button" role="tab">العربية</button>
                </li>
                <li class="nav-item" role="presentation">
                  <button class="nav-link" data-tab="english-tab" type="button" role="tab">English</button>
                </li>
              </ul>
              <div class="tab-content" id="tabs-175Content">
                <div class="tab-pane fade show active arabic-tab" role="tabpanel">
                  <div class="form-group">
                    <label for="input-306">الإسم</label>
                    <input class="form-control" type="text" id="input-306" placeholder="أكتب الإسم"/>
                  </div>
                </div>
                <div class="tab-pane fade english-tab" role="tabpanel">
                  <div class="form-group">
                    <label for="input-54">الإسم</label>
                    <input class="form-control" type="text" id="input-54" placeholder="أكتب الإسم"/>
                  </div>
                </div>
              </div>
            </div>
            <div class="row row-cols-1 row-cols-md-2">
              <div class="col">
                <div class="form-group">
                  <label for="input-652">القسم الرئيسي</label>
                  <select class="niceselect" type="text" id="select-652">
                    <option value="إختر">إختر  </option>
                    <option value="إختر">إختر  </option>
                    <option value="إختر">إختر  </option>
                    <option value="إختر">إختر  </option>
                  </select>
                </div><a class="text-danger text-decoration-none mb-4 d-block" href="" style="margin-top:-15px">تصنيف غير موجود (إضافة تصنيف جديد)</a>
              </div>
              <div class="col">
                <div class="form-group">
                  <label for="input-319">القسم الفرعي</label>
                  <select class="niceselect" type="text" id="select-319">
                    <option value="إختر">إختر  </option>
                    <option value="إختر">إختر  </option>
                    <option value="إختر">إختر  </option>
                    <option value="إختر">إختر  </option>
                  </select>
                </div>
              </div>
              <div class="col">
                <div class="form-group">
                  <label for="input-768">حجم المنتج</label>
                  <input class="form-control" type="number" id="input-768"/>
                  <div class="value-text">كجم</div>
                </div>
              </div>
              <div class="col">
                <div class="form-group">
                  <label for="TradeMark"> العلامة التجارية</label>
                  <div class="cus-multi-select" id="TradeMark" data-options="[{&quot;label&quot;: &quot;نستلة&quot;,&quot;value&quot;: &quot;Nestle&quot;},{&quot;label&quot;: &quot;جهينة&quot;,&quot;value&quot;: &quot;Juhayna&quot;}]"> </div>
                </div>
              </div>
              <div class="col">
                <div class="input-number">
                  <div class="form-group">
                    <label for="input-14">الكمية المتاحة</label>
                    <div class="input-number-control"><a class="input-increment" href="#"> +</a>
                      <input type="text" id="input-14" placeholder="0" value="0"/><a class="input-decrement" href="#"> -</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col">
                <div class="form-group">
                  <label for="input-927">التكلفة المتوقعة للكمية المطلوبة</label>
                  <input class="form-control" type="text" id="input-927"/>
                  <div class="value-text">شيكل</div>
                </div>
              </div>
            </div>
          </div>
          <div class="add-product-footer">
            <button class="btnn-orange btn btnn ml-2 go-to-step-2 hvr-shadow" type="button"><span>التالي </span>
            </button>
            <button class="btnn-red btn btnn hvr-shadow" type="button"><span>إلغاء </span>
            </button>
          </div>
        </div>
        <div class="tab-pane fade" id="step-2-details" role="tabpanel" aria-labelledby="step-2-details-tab">
          <div class="row">
            <div class="col-lg-9">
              <div class="products mt-3">
                <div class="product add-deal d-flex justify-content-between mb-3">
                  <div class="img"><img src="../../assets/images/products/product-1.png" alt=""/></div>
                  <div>
                    <p class="name m-0">بقالة</p>
                    <h4>ليمون مزارع طازج</h4><span class="number">حجم المنتج 2 كم</span>
                    <p>نستلة - جهينة</p>
                  </div>
                  <div>
                    <p class="m-0">السعر التقريبى</p>
                    <p class="price m-0"> 150 دينار</p>
                  </div>
                  <p class="amount m-0">الكمية 5</p>
                  <div>
                    <svg class="me-3">
                      <use href="../../assets/images/icons/icons.svg#delete"> </use>
                    </svg>
                    <svg>
                      <use href="../../assets/images/icons/icons.svg#edit"> </use>
                    </svg>
                  </div>
                </div>
                <div class="product add-deal d-flex justify-content-between mb-3">
                  <div class="img"><img src="../../assets/images/products/product-1.png" alt=""/></div>
                  <div>
                    <p class="name m-0">بقالة</p>
                    <h4>ليمون مزارع طازج</h4><span class="number">حجم المنتج 2 كم</span>
                    <p>نستلة - جهينة</p>
                  </div>
                  <div>
                    <p class="m-0">السعر التقريبى</p>
                    <p class="price m-0"> 150 دينار</p>
                  </div>
                  <p class="amount m-0">الكمية 5</p>
                  <div>
                    <svg class="me-3">
                      <use href="../../assets/images/icons/icons.svg#delete"> </use>
                    </svg>
                    <svg>
                      <use href="../../assets/images/icons/icons.svg#edit"> </use>
                    </svg>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-9"><a class="load-more-sec mb-5">
                <p>اضافة منتج آخر</p></a></div>
          </div>
          <div class="add-product-footer">
            <button class="btnn-orange btn btnn ml-2 go-to-step-3 hvr-shadow" type="button"><span>التالي </span>
            </button>
            <button class="btnn-red btn btnn hvr-shadow" type="button"><span>إلغاء </span>
            </button>
          </div>
        </div>
        <div class="tab-pane fade" id="step-3-details" role="tabpanel" aria-labelledby="step-3-details-tab">
          <div class="add-product-body">
            <h4 class="my-4">عنوان التوصيل</h4>
            <div class="row w-100 m-0">
              <div class="col-lg-4">
                <div class="cover-point-sec">
                  <div class="form-check form-switch form-switch-md">
                    <input class="form-check-input" type="checkbox" id="switch8" role="switch"/>
                    <label class="form-check-label" for="switch8"></label><img class="bg-image-checked" src="../../assets/images/landing/active-cover-points.png" alt=""/><img class="bg-image" src="../../assets/images/landing/cover-points-box.png" alt=""/>
                    <div class="text">
                      <h2> العنوان الافتراضي</h2>
                      <div class="d-flex justify-content-start align-items-start">
                        <div class="shap">...</div>
                        <div>
                          <h3>التوصيل الى عنوانى الحالى</h3>
                          <h4>اول مكرم ، مدينة نصر   </h4>
                          <h4>القاهرة جمهورية مصر العربية</h4>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="cover-point-sec">
                  <div class="form-check form-switch form-switch-md">
                    <input class="form-check-input" type="checkbox" id="switch8" role="switch"/>
                    <label class="form-check-label" for="switch8"></label><img class="bg-image-checked" src="../../assets/images/landing/active-cover-points.png" alt=""/><img class="bg-image" src="../../assets/images/landing/cover-points-box.png" alt=""/>
                    <div class="text">
                      <h2> العنوان الافتراضي</h2>
                      <div class="d-flex justify-content-start align-items-start">
                        <div class="shap">...</div>
                        <div>
                          <h3>التوصيل الى عنوانى الحالى</h3>
                          <h4>اول مكرم ، مدينة نصر   </h4>
                          <h4>القاهرة جمهورية مصر العربية</h4>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-2"><a class="load-more-sec h-100 m-0">
                  <p>اضافة عنوان جديد
                    <div class="day-time mt-5"> </div>
                  </p></a></div>
            </div>
            <div class="row w-100 m-0">
              <div class="col-md-8">
                <div class="day-time mt-5 mb-3">
                  <h4 class="mb-4">يوم ووقت التوصيل</h4>
                  <div class="d-flex justify-content-between align-items-start">
                    <button class="btn btnn hvr-shadow" data-bs-toggle="modal" data-bs-target="#dayModal">
                      <svg>
                        <use href="../../assets/images/icons/icons.svg#calender"></use>
                      </svg><span>أختر يوم التوصيل</span>
                      <div class="arrow">
                        <svg>
                          <use href="../../assets/images/icons/icons.svg#arrow-3"></use>
                        </svg>
                      </div>
                    </button>
                    <button class="btn btnn hvr-shadow" data-bs-toggle="modal" data-bs-target="#timeModal">
                      <svg>
                        <use href="../../assets/images/icons/icons.svg#clock"></use>
                      </svg><span>أختر وقت التوصيل</span>
                      <div class="arrow">
                        <svg>
                          <use href="../../assets/images/icons/icons.svg#arrow-3"></use>
                        </svg>
                      </div>
                    </button>
                  </div>
                </div>
                <div class="input-number">
                  <div class="form-group">
                    <label for="input-121">مدة استقبال العروض لهذه الصفقة</label>
                    <div class="input-number-control"><a class="input-increment" href="#"> +</a>
                      <input type="text" id="input-121" placeholder="0" value="0"/><a class="input-decrement" href="#"> -</a>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="input-900">ملاحظاتك </label>
                  <textarea class="form-control" rows="5"></textarea>
                </div>
              </div>
            </div>
          </div>
          <div class="add-product-footer"><a class="btnn-orange btn btnn hvr-shadow btn-fill" href="{{route('check-out-deal')}}"><span>اضافة صفقة</span></a>
            <button class="btnn-red btn btnn hvr-shadow" type="button"><span>إلغاء </span>
            </button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="dayModal" tabindex="-1" aria-labelledby="dayModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content time-modal">
          <div class="modal-header">
            <h5 class="modal-title" id="timeModalLabel"> </h5>
            <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body p-5">
            <div class="cansel-offer-modal p-0"> <span class="close" data-bs-toggle="modal" data-bs-target="#dayModal">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#close"></use>
                </svg></span>
              <h5 class="fw-bold">معاد التوصيل</h5>
              <ul class="p-0 m-0 w-100 payments-list-check mt-4">
                <li class="me-md-2">
                  <input class="active" type="radio" name="payment" id="day-1">
                  <label class="active" for="day-1">
                    <div class="title">اليوم 03/03/2022</div>
                    <div class="express">
                      <p class="m-0 fw-bold">Express</p><span>SR 20+</span><img class="img" src="../../assets/images/landing/express.svg" alt="">
                    </div>
                  </label>
                </li>
                <li>
                  <input type="radio" name="payment" id="day-2">
                  <label for="day-2">
                    <div class="title">غدا 22/4/2021</div>
                  </label>
                </li>
                <li class="me-md-2">
                  <input type="radio" name="payment" id="day-3">
                  <label for="day-3">
                    <div class="title">الثلاثاء 22/4/2021</div>
                  </label>
                </li>
                <li>
                  <input type="radio" name="payment" id="day-4">
                  <label for="day-4">
                    <div class="title">الأربعاء 22/4/2021</div>
                  </label>
                </li>
              </ul>
              <button class="w-100 btnn-orange btn-fill btn btnn hvr-shadow" data-bs-toggle="modal" data-bs-target="#dayModal"><span>تم </span></button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="timeModal" tabindex="-1" aria-labelledby="timeModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content time-modal">
          <div class="modal-header">
            <h5 class="modal-title" id="timeModalLabel"> </h5>
            <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body p-5">
            <div class="cansel-offer-modal p-0"> <span class="close" data-bs-toggle="modal" data-bs-target="#timeModal">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#close"></use>
                </svg></span>
              <h5 class="fw-bold">وقت التوصيل</h5>
              <ul class="p-0 m-0 w-100 payments-list-check mt-4">
                <li class="me-md-2">
                  <input class="active" type="radio" name="payment" id="time-1">
                  <label class="active" for="time-1">
                    <div class="title">10-12 صباحاَ</div>
                  </label>
                </li>
                <li>
                  <input type="radio" name="payment" id="time-2">
                  <label for="time-2">
                    <div class="title">10-12 صباحاَ</div>
                  </label>
                </li>
                <li class="me-md-2">
                  <input type="radio" name="payment" id="time-3">
                  <label for="time-3">
                    <div class="title">10-12 صباحاَ </div>
                  </label>
                </li>
                <li>
                  <input type="radio" name="payment" id="time-4">
                  <label for="time-4">
                    <div class="title">10-12 صباحاَ</div>
                  </label>
                </li>
                <li>
                  <input type="radio" name="payment" id="time-4">
                  <label for="time-4">
                    <div class="title">10-12 صباحاَ</div>
                  </label>
                </li>
              </ul>
              <button class="w-100 btnn-orange btn-fill btn btnn hvr-shadow" data-bs-toggle="modal" data-bs-target="#timeModal"><span>تم</span></button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection

