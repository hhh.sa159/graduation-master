@extends('landing.parent')

@section('Home-Title','Product')

@section('add-deal')

<div class="landing-notis-head d-flex justify-content-center align-items-center">
    <p class="m-0">هل تريد ان تبدء صفقة جديدة الان ؟ </p><a class="btnn-orange btn btnn hvr-shadow btn-fill" href="{{route('add-deal')}}"><span>اضافة صفقة</span></a>
  </div>

@endsection

@section('main-content')

<div class="main-content pb-5">
    <div class="header">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{route('home')}}">الرئيسية</a></li>
          <li class="breadcrumb-item"><a href="#">البقالة</a></li>
          <li class="breadcrumb-item">الجبن</li>
        </ol>
      </nav>
    </div>
    <div class="product-details mt-5">
      <div class="row w-100 m-0">
        <div class="col-md-6 px-5">
          <div class="images-side">
            <div class="fotorama" data-loop="true" data-allowfullscreen="true" data-autoplay="true" data-nav="thumbs" data-thumbwidth="200" data-thumbheight="200" data-thumbmargin="20" data-thumbborderwidth="1"><img src="../../assets/images/product-details/details-1.png" alt=""><img src="../../assets/images/product-details/details-2.png" alt=""><img src="../../assets/images/product-details/details-3.png" alt=""><img src="../../assets/images/product-details/details-4.png" alt=""><img src="../../assets/images/product-details/details-1.png" alt=""><img src="../../assets/images/product-details/details-2.png" alt=""><img src="../../assets/images/product-details/details-3.png" alt=""><img src="../../assets/images/product-details/details-4.png" alt=""></div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="details-side">
            <div class="details-header d-flex justify-content-between mb-3">
              <div class="d-flex justify-content-start align-items-center">
                <p class="mb-0">منتجات الألبان </p>
                <p class="mb-0"> جبن</p>
              </div>
              <div class="icon d-flex justify-content-center align-items-center">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#heart-fill"></use>
                </svg>
              </div>
            </div>
            <div class="details-content"><a href="#">Grocery shope</a>
              <h2 class="my-3">جبنة كريمي سبريد كيري - 500 جم</h2>
              <p class="mb-0">صندوق 50 كيلو جرام</p>
              <div class="d-flex justify-content-start align-items-center mt-3">
                <p class="price m-0">350 <span>شيكل</span><span class="old-price">50 دينار</span></p>
                <p class="mb-0 discount">
                  <svg>
                    <use href="../../assets/images/icons/icons.svg#heart"></use>
                  </svg>25 % خصم
                </p>
              </div>
              <p class="mb-0 mt-3">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام "هنا يوجد هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام </p>
              <p class="mb-0 mt-4 amount">تبقى 5 قطع فقط</p>
              <div class="input-margin d-flex justify-content-start align-items-center mt-3">
                <div class="input-number">
                  <div class="form-group">
                    <div class="input-number-control"><a class="input-increment" href="#"> +</a>
                      <input type="text" id="input-572" placeholder="0" value="0"/><a class="input-decrement" href="#"> -</a>
                    </div>
                  </div>
                </div><a class="mb-0 add-cart d-flex justify-content-between align-items-center" href="{{route('cart')}}">
                  <span>اضافة للسلة</span><span class="font-bold">350 شيكل</span></a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="add-product-title mt-5">
        <h2>مواصفات المنتج</h2>
      </div>
      <div class="order-details">
        <div class="order-total">
          <ul class="m-0">
            <li>
              <h5>الحجم </h5>
              <h6>50 كيلو جرام</h6>
            </li>
            <li>
              <h5>النوع</h5>
              <h6>مزارع</h6>
            </li>
            <li>
              <h5>التاجر </h5>
              <h6> <a>Grocery shope</a></h6>
            </li>
            <li>
              <h5>مواد حافظة</h5>
              <h6>لا</h6>
            </li>
            <li>
              <h5>الحجم</h5>
              <h6>50 كيلو جرام</h6>
            </li>
          </ul>
        </div>
      </div>
      <div class="add-product-title mt-5">
        <h2>جدول الكمية<span>كل مازادت الكمية قل السعر</span></h2>
      </div>
      <div class="amount-table">
        <div class="row w-100 m-0">
          <div class="col-6 mb-4">
            <h5 class="fw-bold">100 شيكل</h5><span>5-55 حبة</span>
          </div>
          <div class="col-6 mb-4">
            <h5 class="fw-bold">100 شيكل</h5><span>5-55 حبة</span>
          </div>
          <div class="col-6 mb-4">
            <h5 class="fw-bold">100 شيكل</h5><span>5-55 حبة</span>
          </div>
          <div class="col-6 mb-4">
            <h5 class="fw-bold">100 شيكل</h5><span>5-55 حبة</span>
          </div>
          <div class="col-12"> <span>السعر الاساسى 85 شيكل</span></div>
        </div>
      </div>
      <div class="add-product-title mt-5">
        <h2>منتجات مشابهة</h2>
      </div>
      <div class="row m-0 w-100 mt-5 row-cols-1 row-cols-sm-2 row-cols-md-4">
        <div class="col"> <a class="product hvr-float-shadow" href="{{route('product')}}">
            <div>
              <div class="icon d-flex justify-content-center align-items-center">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#heart-fill"></use>
                </svg>
              </div><span class="discount">- 10%</span>
              <div>
                <div class="img"><img src="../../assets/images/products/product-1.png" alt=""/></div>
              </div>
              <div>
                <p class="name">اسم التاجر</p>
                <h4>جبنة لافاش كيري</h4>
                <p class="price">150 دينار<span class="old-price">50 دينار</span></p><span class="number">48 قطعة</span>
              </div>
              <button class="plus">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#plus"></use>
                </svg>
              </button>
            </div></a>
        </div>
        <div class="col">  <a class="product hvr-float-shadow" href="{{route('product')}}">
            <div>
              <div class="icon d-flex justify-content-center align-items-center">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#heart-fill"></use>
                </svg>
              </div>
              <div>
                <div class="img"><img src="../../assets/images/products/product-2.png" alt=""/></div>
              </div>
              <div>
                <p class="name">اسم التاجر</p>
                <h4>جبنة لافاش كيري</h4>
                <p class="price">150 دينار<span class="old-price">50 دينار</span></p><span class="number">48 قطعة</span>
              </div>
              <button class="plus">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#plus"></use>
                </svg>
              </button>
            </div></a>
        </div>
        <div class="col">  <a class="product hvr-float-shadow" href="{{route('product')}}">
            <div>
              <div class="icon d-flex justify-content-center align-items-center">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#heart-fill"></use>
                </svg>
              </div><span class="discount">- 10%</span>
              <div>
                <div class="img"><img src="../../assets/images/products/product-3.png" alt=""/></div>
              </div>
              <div>
                <p class="name">اسم التاجر</p>
                <h4>جبنة لافاش كيري</h4>
                <p class="price">150 دينار<span class="old-price">50 دينار</span></p><span class="number">48 قطعة</span>
              </div>
              <button class="plus">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#plus"></use>
                </svg>
              </button>
            </div></a>
        </div>
        <div class="col">  <a class="product hvr-float-shadow" href="{{route('product')}}">
            <div>
              <div class="icon d-flex justify-content-center align-items-center">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#heart-fill"></use>
                </svg>
              </div><span class="discount">- 10%</span>
              <div>
                <div class="img"><img src="../../assets/images/products/product-4.png" alt=""/></div>
              </div>
              <div>
                <p class="name">اسم التاجر</p>
                <h4>جبنة لافاش كيري</h4>
                <p class="price">150 دينار<span class="old-price">50 دينار</span></p><span class="number">48 قطعة</span>
              </div>
              <button class="plus">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#plus"></use>
                </svg>
              </button>
            </div></a>
        </div>
        <div class="col">  <a class="product hvr-float-shadow" href="{{route('product')}}">
            <div>
              <div class="icon d-flex justify-content-center align-items-center">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#heart-fill"></use>
                </svg>
              </div><span class="discount">- 10%</span>
              <div>
                <div class="img"><img src="../../assets/images/products/product-5.png" alt=""/></div>
              </div>
              <div>
                <p class="name">اسم التاجر</p>
                <h4>جبنة لافاش كيري</h4>
                <p class="price">150 دينار<span class="old-price">50 دينار</span></p><span class="number">48 قطعة</span>
              </div>
              <button class="plus">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#plus"></use>
                </svg>
              </button>
            </div></a>
        </div>
        <div class="col">  <a class="product hvr-float-shadow" href="{{route('product')}}">
            <div>
              <div class="icon d-flex justify-content-center align-items-center">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#heart-fill"></use>
                </svg>
              </div>
              <div>
                <div class="img"><img src="../../assets/images/products/product-1.png" alt=""/></div>
              </div>
              <div>
                <p class="name">اسم التاجر</p>
                <h4>جبنة لافاش كيري</h4>
                <p class="price">150 دينار<span class="old-price">50 دينار</span></p><span class="number">48 قطعة</span>
              </div>
              <button class="plus">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#plus"></use>
                </svg>
              </button>
            </div></a>
        </div>
        <div class="col">  <a class="product hvr-float-shadow" href="{{route('product')}}">
            <div>
              <div class="icon d-flex justify-content-center align-items-center">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#heart-fill"></use>
                </svg>
              </div><span class="discount">- 10%</span>
              <div>
                <div class="img"><img src="../../assets/images/products/product-2.png" alt=""/></div>
              </div>
              <div>
                <p class="name">اسم التاجر</p>
                <h4>جبنة لافاش كيري</h4>
                <p class="price">150 دينار<span class="old-price">50 دينار</span></p><span class="number">48 قطعة</span>
              </div>
              <button class="plus">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#plus"></use>
                </svg>
              </button>
            </div></a>
        </div>
        <div class="col">  <a class="product hvr-float-shadow" href="{{route('product')}}">
            <div>
              <div class="icon d-flex justify-content-center align-items-center">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#heart-fill"></use>
                </svg>
              </div><span class="discount">- 10%</span>
              <div>
                <div class="img"><img src="../../assets/images/products/product-3.png" alt=""/></div>
              </div>
              <div>
                <p class="name">اسم التاجر</p>
                <h4>جبنة لافاش كيري</h4>
                <p class="price">150 دينار<span class="old-price">50 دينار</span></p><span class="number">48 قطعة</span>
              </div>
              <button class="plus">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#plus"></use>
                </svg>
              </button>
            </div></a>
        </div>
      </div>
    </div>
  </div>

@endsection
