@extends('landing.parent')

@section('Home-Title','Home')

@section('add-deal')

<div class="landing-notis-head d-flex justify-content-center align-items-center">
    {{-- <p class="m-0">هل تريد ان تبدء صفقة جديدة الان ؟ </p><a class="btnn-orange btn btnn hvr-shadow btn-fill" href="{{route('add-deal')}}"><span>اضافة صفقة</span></a> --}}
  </div>

@endsection

@section('main-content')

<div class="main-content pb-5">
    <div class="home-slider">
      <div class="owl-carousel owl-theme">
        <div class="itme"> <img src="../../assets/images/landing/home-slide-1.png" alt=""></div>
        <div class="itme"> <img src="../../assets/images/landing/home-slide-1.png" alt=""></div>
        <div class="itme"> <img src="../../assets/images/landing/home-slide-1.png" alt=""></div>
      </div>
    </div>
    <div class="product-type-list mt-5">
      <div class="owl-carousel owl-theme">
        <div class="item product-type">
          <input type="checkbox" name="prduct_type0">
          <div class="image"> <img src="../../assets/images/products-types/type-1.png" alt=""></div>
          <h3>خضراوات </h3>
        </div>
        <div class="item product-type">
          <input type="checkbox" name="prduct_type1">
          <div class="image"> <img src="../../assets/images/products-types/type-2.png" alt=""></div>
          <h3>مخبوزات </h3>
        </div>
        <div class="item product-type">
          <input type="checkbox" name="prduct_type2">
          <div class="image"> <img src="../../assets/images/products-types/type-3.png" alt=""></div>
          <h3>الدجاج </h3>
        </div>
        <div class="item product-type">
          <input type="checkbox" name="prduct_type3">
          <div class="image"> <img src="../../assets/images/products-types/type-4.png" alt=""></div>
          <h3>حلويات وبسكويت </h3>
        </div>
        <div class="item product-type">
          <input type="checkbox" name="prduct_type4">
          <div class="image"> <img src="../../assets/images/products-types/type-5.png" alt=""></div>
          <h3>وجبات خفيفة </h3>
        </div>
        <div class="item product-type">
          <input type="checkbox" name="prduct_type5">
          <div class="image"> <img src="../../assets/images/products-types/type-6.png" alt=""></div>
          <h3>البقالة </h3>
        </div>
        <div class="item product-type">
          <input type="checkbox" name="prduct_type6">
          <div class="image"> <img src="../../assets/images/products-types/type-7.png" alt=""></div>
          <h3>معلبات </h3>
        </div>
        <div class="item product-type">
          <input type="checkbox" name="prduct_type7">
          <div class="image"> <img src="../../assets/images/products-types/type-8.png" alt=""></div>
          <h3>مجمدات </h3>
        </div>
        <div class="item product-type">
          <input type="checkbox" name="prduct_type8">
          <div class="image"> <img src="../../assets/images/products-types/type-9.png" alt=""></div>
          <h3>العناية بالطفل </h3>
        </div>
        <div class="item product-type">
          <input type="checkbox" name="prduct_type9">
          <div class="image"> <img src="../../assets/images/products-types/type-10.png" alt=""></div>
          <h3>العناية الشخصية </h3>
        </div>
        <div class="item product-type">
          <input type="checkbox" name="prduct_type10">
          <div class="image"> <img src="../../assets/images/products-types/type-11.png" alt=""></div>
          <h3>منتجات الألبان </h3>
        </div>
      </div>
    </div>
    <div class="products mt-5">
      <div class="home-sec-head mb-4">
        <div class="text">
          {{-- <h4> وصل حديثا فى قسم البقالة</h4><a class="link" href="{{route('product-list')}}">زيارة القسم --}}
            <svg>
              <use href="../../assets/images/icons/icons.svg#arrow-right"></use>
            </svg></a>
        </div>
        {{-- <div class="button"> <a class="btnn-orange btn btnn hvr-shadow" href="{{route('product-list')}}"><span>زيارة القسم</span></a></div> --}}
      </div>
      <div class="row m-0 w-100 mt-5 row-cols-1 row-cols-sm-2 row-cols-md-4">
        {{-- <div class="col"> <a class="product hvr-float-shadow" href="{{route('product')}}"> --}}
            <div>
              <div class="icon d-flex justify-content-center align-items-center">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#heart-fill"></use>
                </svg>
              </div><span class="discount">- 10%</span>
              <div>
                <div class="img"><img src="../../assets/images/products/product-1.png" alt=""/></div>
              </div>
              <div>
                <p class="name">اسم التاجر</p>
                <h4>جبنة لافاش كيري</h4>
                <p class="price">150 دينار<span class="old-price">50 دينار</span></p><span class="number">48 قطعة</span>
              </div>
              <button class="plus">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#plus"></use>
                </svg>
              </button>
            </div></a>
        </div>
        {{-- <div class="col">  <a class="product hvr-float-shadow" href="{{route('product')}}"> --}}
            <div>
              <div class="icon d-flex justify-content-center align-items-center">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#heart-fill"></use>
                </svg>
              </div>
              <div>
                <div class="img"><img src="../../assets/images/products/product-2.png" alt=""/></div>
              </div>
              <div>
                <p class="name">اسم التاجر</p>
                <h4>جبنة لافاش كيري</h4>
                <p class="price">150 دينار<span class="old-price">50 دينار</span></p><span class="number">48 قطعة</span>
              </div>
              <button class="plus">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#plus"></use>
                </svg>
              </button>
            </div></a>
        </div>
        {{-- <div class="col">  <a class="product hvr-float-shadow" href="{{route('product')}}"> --}}
            <div>
              <div class="icon d-flex justify-content-center align-items-center">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#heart-fill"></use>
                </svg>
              </div><span class="discount">- 10%</span>
              <div>
                <div class="img"><img src="../../assets/images/products/product-3.png" alt=""/></div>
              </div>
              <div>
                <p class="name">اسم التاجر</p>
                <h4>جبنة لافاش كيري</h4>
                <p class="price">150 دينار<span class="old-price">50 دينار</span></p><span class="number">48 قطعة</span>
              </div>
              <button class="plus">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#plus"></use>
                </svg>
              </button>
            </div></a>
        </div>
        {{-- <div class="col">  <a class="product hvr-float-shadow" href="{{route('product')}}"> --}}
            <div>
              <div class="icon d-flex justify-content-center align-items-center">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#heart-fill"></use>
                </svg>
              </div><span class="discount">- 10%</span>
              <div>
                <div class="img"><img src="../../assets/images/products/product-4.png" alt=""/></div>
              </div>
              <div>
                <p class="name">اسم التاجر</p>
                <h4>جبنة لافاش كيري</h4>
                <p class="price">150 دينار<span class="old-price">50 دينار</span></p><span class="number">48 قطعة</span>
              </div>
              <button class="plus">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#plus"></use>
                </svg>
              </button>
            </div></a>
        </div>
        {{-- <div class="col">  <a class="product hvr-float-shadow" href="{{route('product')}}"> --}}
            <div>
              <div class="icon d-flex justify-content-center align-items-center">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#heart-fill"></use>
                </svg>
              </div><span class="discount">- 10%</span>
              <div>
                <div class="img"><img src="../../assets/images/products/product-5.png" alt=""/></div>
              </div>
              <div>
                <p class="name">اسم التاجر</p>
                <h4>جبنة لافاش كيري</h4>
                <p class="price">150 دينار<span class="old-price">50 دينار</span></p><span class="number">48 قطعة</span>
              </div>
              <button class="plus">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#plus"></use>
                </svg>
              </button>
            </div></a>
        </div>
        {{-- <div class="col">  <a class="product hvr-float-shadow" href="{{route('product')}}"> --}}
            <div>
              <div class="icon d-flex justify-content-center align-items-center">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#heart-fill"></use>
                </svg>
              </div>
              <div>
                <div class="img"><img src="../../assets/images/products/product-1.png" alt=""/></div>
              </div>
              <div>
                <p class="name">اسم التاجر</p>
                <h4>جبنة لافاش كيري</h4>
                <p class="price">150 دينار<span class="old-price">50 دينار</span></p><span class="number">48 قطعة</span>
              </div>
              <button class="plus">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#plus"></use>
                </svg>
              </button>
            </div></a>
        </div>
        {{-- <div class="col">  <a class="product hvr-float-shadow" href="{{route('product')}}"> --}}
            <div>
              <div class="icon d-flex justify-content-center align-items-center">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#heart-fill"></use>
                </svg>
              </div><span class="discount">- 10%</span>
              <div>
                <div class="img"><img src="../../assets/images/products/product-2.png" alt=""/></div>
              </div>
              <div>
                <p class="name">اسم التاجر</p>
                <h4>جبنة لافاش كيري</h4>
                <p class="price">150 دينار<span class="old-price">50 دينار</span></p><span class="number">48 قطعة</span>
              </div>
              <button class="plus">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#plus"></use>
                </svg>
              </button>
            </div></a>
        </div>
        {{-- <div class="col">  <a class="product hvr-float-shadow" href="{{route('product')}}"> --}}
            <div>
              <div class="icon d-flex justify-content-center align-items-center">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#heart-fill"></use>
                </svg>
              </div><span class="discount">- 10%</span>
              <div>
                <div class="img"><img src="../../assets/images/products/product-3.png" alt=""/></div>
              </div>
              <div>
                <p class="name">اسم التاجر</p>
                <h4>جبنة لافاش كيري</h4>
                <p class="price">150 دينار<span class="old-price">50 دينار</span></p><span class="number">48 قطعة</span>
              </div>
              <button class="plus">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#plus"></use>
                </svg>
              </button>
            </div></a>
        </div>
      </div>
    </div>
    <div class="img-section mt-5"> <img src="../../assets/images/landing/img-1.png" alt=""></div>
    <div class="bargains mt-5">
      <div class="home-sec-head mb-4">
        <div class="text">
          <h4> الصفقات الحديثة</h4>
          <p> أحدث الصفقات الي طلبها المستخدمين</p>
        </div>
        {{-- <div class="button"> <a class="btnn-orange btn btnn hvr-shadow" href="{{route('product-list')}}"><span>زيارة القسم</span></a></div> --}}
      </div>
      <div class="row m-0 w-100 mt-5">
        <div class="col-md-4">
          <div class="bargains-img">
            <p>أحدث صفقات البقالة</p>
          </div>
        </div>
        <div class="col-md-8">
          <div class="all-bargins">
            <p class="mt-3"><a href="#">عرض جميع الصفقات لهذا القسم
                <svg>
                  <use href="../../assets/images/icons/icons.svg#arrow-right"></use>
                </svg></a></p>
            <div class="row m-0 w-100 mt-4">
              {{-- <div class="col-md-6 mb-4"><a class="bargain hvr-float-shadow" href="{{route('order-details')}}"> --}}
                  <div class="bargain-head">
                    <h6 class="m-0">حالة الطلب</h6><span> يتم التوصيل </span>
                  </div>
                  <div class="bargain-body">
                    <div class="row w-100 m-0">
                      <div class="col-6">
                        <div class="bargain-body-sec"><span>إجمالي الطلب</span>
                          <h6>230 شيكل</h6>
                        </div>
                      </div>
                      <div class="col-6">
                        <div class="bargain-body-sec"><span>التاجر</span>
                          <h6>Grocery shope</h6>
                        </div>
                      </div>
                      <div class="col-6">
                        <div class="bargain-body-sec"><span>وقت وتاريخ الطلب</span>
                          <h6>20/10/2021 - 10:50 م</h6>
                        </div>
                      </div>
                    </div>
                  </div></a>
              </div>
              {{-- <div class="col-md-6 mb-4"><a class="bargain hvr-float-shadow" href="{{route('order-details')}}"> --}}
                  <div class="bargain-head">
                    <h6 class="m-0">حالة الطلب</h6><span> يتم التوصيل </span>
                  </div>
                  <div class="bargain-body">
                    <div class="row w-100 m-0">
                      <div class="col-6">
                        <div class="bargain-body-sec"><span>إجمالي الطلب</span>
                          <h6>230 شيكل</h6>
                        </div>
                      </div>
                      <div class="col-6">
                        <div class="bargain-body-sec"><span>التاجر</span>
                          <h6>Grocery shope</h6>
                        </div>
                      </div>
                      <div class="col-6">
                        <div class="bargain-body-sec"><span>وقت وتاريخ الطلب</span>
                          <h6>20/10/2021 - 10:50 م</h6>
                        </div>
                      </div>
                    </div>
                  </div></a>
              </div>
              {{-- <div class="col-md-6 mb-4"><a class="bargain hvr-float-shadow" href="{{route('order-details')}}"> --}}
                  <div class="bargain-head">
                    <h6 class="m-0">حالة الطلب</h6><span> يتم التوصيل </span>
                  </div>
                  <div class="bargain-body">
                    <div class="row w-100 m-0">
                      <div class="col-6">
                        <div class="bargain-body-sec"><span>إجمالي الطلب</span>
                          <h6>230 شيكل</h6>
                        </div>
                      </div>
                      <div class="col-6">
                        <div class="bargain-body-sec"><span>التاجر</span>
                          <h6>Grocery shope</h6>
                        </div>
                      </div>
                      <div class="col-6">
                        <div class="bargain-body-sec"><span>وقت وتاريخ الطلب</span>
                          <h6>20/10/2021 - 10:50 م</h6>
                        </div>
                      </div>
                    </div>
                  </div></a>
              </div>
              {{-- <div class="col-md-6 mb-4"><a class="bargain hvr-float-shadow" href="{{route('order-details')}}"> --}}
                  <div class="bargain-head">
                    <h6 class="m-0">حالة الطلب</h6><span> يتم التوصيل </span>
                  </div>
                  <div class="bargain-body">
                    <div class="row w-100 m-0">
                      <div class="col-6">
                        <div class="bargain-body-sec"><span>إجمالي الطلب</span>
                          <h6>230 شيكل</h6>
                        </div>
                      </div>
                      <div class="col-6">
                        <div class="bargain-body-sec"><span>التاجر</span>
                          <h6>Grocery shope</h6>
                        </div>
                      </div>
                      <div class="col-6">
                        <div class="bargain-body-sec"><span>وقت وتاريخ الطلب</span>
                          <h6>20/10/2021 - 10:50 م</h6>
                        </div>
                      </div>
                    </div>
                  </div></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="electronics mt-5">
      <div class="home-sec-head mb-4">
        <div class="text">
          {{-- <h4> حديثا فى قسم الالكترونيات</h4><a class="link" href="{{route('product-list')}}">زيارة القسم --}}
            <svg>
              <use href="../../assets/images/icons/icons.svg#arrow-right"></use>
            </svg></a>
        </div>
        {{-- <div class="button"> <a class="btnn-orange btn btnn hvr-shadow" href="{{route('product-list')}}"><span>زيارة القسم</span></a></div> --}}
      </div>
      <div class="row m-0 w-100 mt-5 row-cols-1 row-cols-sm-2 row-cols-md-4">
        {{-- <div class="col"> <a class="product hvr-float-shadow" href="{{route('product')}}"> --}}
            <div>
              <div class="icon d-flex justify-content-center align-items-center">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#heart-fill"></use>
                </svg>
              </div><span class="discount">- 10%</span>
              <div>
                <div class="img"><img src="../../assets/images/landing/electronic/electronic-1.png" alt=""/></div>
              </div>
              <div>
                <p class="name">اسم التاجر</p>
                <h4>جبنة لافاش كيري</h4>
                <p class="price">150 دينار<span class="old-price">50 دينار</span></p><span class="number">48 قطعة</span>
              </div>
              <button class="plus">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#plus"></use>
                </svg>
              </button>
            </div></a>
        </div>
        {{-- <div class="col">  <a class="product hvr-float-shadow" href="{{route('product')}}"> --}}
            <div>
              <div class="icon d-flex justify-content-center align-items-center">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#heart-fill"></use>
                </svg>
              </div>
              <div>
                <div class="img"><img src="../../assets/images/landing/electronic/electronic-2.png" alt=""/></div>
              </div>
              <div>
                <p class="name">اسم التاجر</p>
                <h4>جبنة لافاش كيري</h4>
                <p class="price">150 دينار<span class="old-price">50 دينار</span></p><span class="number">48 قطعة</span>
              </div>
              <button class="plus">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#plus"></use>
                </svg>
              </button>
            </div></a>
        </div>
        {{-- <div class="col">  <a class="product hvr-float-shadow" href="{{route('product')}}"> --}}
            <div>
              <div class="icon d-flex justify-content-center align-items-center">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#heart-fill"></use>
                </svg>
              </div><span class="discount">- 10%</span>
              <div>
                <div class="img"><img src="../../assets/images/landing/electronic/electronic-3.png" alt=""/></div>
              </div>
              <div>
                <p class="name">اسم التاجر</p>
                <h4>جبنة لافاش كيري</h4>
                <p class="price">150 دينار<span class="old-price">50 دينار</span></p><span class="number">48 قطعة</span>
              </div>
              <button class="plus">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#plus"></use>
                </svg>
              </button>
            </div></a>
        </div>
        {{-- <div class="col">  <a class="product hvr-float-shadow" href="{{route('product')}}"> --}}
            <div>
              <div class="icon d-flex justify-content-center align-items-center">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#heart-fill"></use>
                </svg>
              </div><span class="discount">- 10%</span>
              <div>
                <div class="img"><img src="../../assets/images/landing/electronic/electronic-4.png" alt=""/></div>
              </div>
              <div>
                <p class="name">اسم التاجر</p>
                <h4>جبنة لافاش كيري</h4>
                <p class="price">150 دينار<span class="old-price">50 دينار</span></p><span class="number">48 قطعة</span>
              </div>
              <button class="plus">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#plus"></use>
                </svg>
              </button>
            </div></a>
        </div>
        {{-- <div class="col">  <a class="product hvr-float-shadow" href="{{route('product')}}"> --}}
            <div>
              <div class="icon d-flex justify-content-center align-items-center">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#heart-fill"></use>
                </svg>
              </div><span class="discount">- 10%</span>
              <div>
                <div class="img"><img src="../../assets/images/landing/electronic/electronic-5.png" alt=""/></div>
              </div>
              <div>
                <p class="name">اسم التاجر</p>
                <h4>جبنة لافاش كيري</h4>
                <p class="price">150 دينار<span class="old-price">50 دينار</span></p><span class="number">48 قطعة</span>
              </div>
              <button class="plus">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#plus"></use>
                </svg>
              </button>
            </div></a>
        </div>
        {{-- <div class="col">  <a class="product hvr-float-shadow" href="{{route('product')}}"> --}}
            <div>
              <div class="icon d-flex justify-content-center align-items-center">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#heart-fill"></use>
                </svg>
              </div>
              <div>
                <div class="img"><img src="../../assets/images/landing/electronic/electronic-1.png" alt=""/></div>
              </div>
              <div>
                <p class="name">اسم التاجر</p>
                <h4>جبنة لافاش كيري</h4>
                <p class="price">150 دينار<span class="old-price">50 دينار</span></p><span class="number">48 قطعة</span>
              </div>
              <button class="plus">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#plus"></use>
                </svg>
              </button>
            </div></a>
        </div>
        {{-- <div class="col">  <a class="product hvr-float-shadow" href="{{route('product')}}"> --}}
            <div>
              <div class="icon d-flex justify-content-center align-items-center">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#heart-fill"></use>
                </svg>
              </div><span class="discount">- 10%</span>
              <div>
                <div class="img"><img src="../../assets/images/landing/electronic/electronic-2.png" alt=""/></div>
              </div>
              <div>
                <p class="name">اسم التاجر</p>
                <h4>جبنة لافاش كيري</h4>
                <p class="price">150 دينار<span class="old-price">50 دينار</span></p><span class="number">48 قطعة</span>
              </div>
              <button class="plus">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#plus"></use>
                </svg>
              </button>
            </div></a>
        </div>
        {{-- <div class="col">  <a class="product hvr-float-shadow" href="{{route('product')}}"> --}}
            <div>
              <div class="icon d-flex justify-content-center align-items-center">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#heart-fill"></use>
                </svg>
              </div><span class="discount">- 10%</span>
              <div>
                <div class="img"><img src="../../assets/images/landing/electronic/electronic-3.png" alt=""/></div>
              </div>
              <div>
                <p class="name">اسم التاجر</p>
                <h4>جبنة لافاش كيري</h4>
                <p class="price">150 دينار<span class="old-price">50 دينار</span></p><span class="number">48 قطعة</span>
              </div>
              <button class="plus">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#plus"></use>
                </svg>
              </button>
            </div></a>
        </div>
      </div>
    </div>
    <div class="img-section mt-5"> <img src="../../assets/images/landing/img-1.png" alt=""></div>
    <div class="stories-marchants mt-5">
      <div class="home-sec-head mb-4">
        <div class="text">
          <h4>  عروض التجار</h4>
        </div>
        {{-- <div class="button"> <a class="btnn-orange btn btnn hvr-shadow" href="{{route('product-list')}}"><span>زيارة القسم</span></a></div> --}}
      </div>
    </div>
    <div class="offers mt-5"> <a class="offer hvr-float-shadow" href="#">
        <div class="offer-image"><img src="../../assets/images/landing/offer/offer-1.png" alt=""/></div>
        <div class="offer-body">
          <h4 class="mb-2">الإلكترونيات</h4>
          <p class="m-0">خصم 50%</p>
        </div></a><a class="offer hvr-float-shadow" href="#">
        <div class="offer-image"><img src="../../assets/images/landing/offer/offer-2.png" alt=""/></div>
        <div class="offer-body">
          <h4 class="mb-2">البقالة</h4>
          <p class="m-0">خصم 20%</p>
        </div></a><a class="offer hvr-float-shadow" href="#">
        <div class="offer-image"><img src="../../assets/images/landing/offer/offer-3.png" alt=""/></div>
        <div class="offer-body">
          <h4 class="mb-2">العناية بالبشرة</h4>
          <p class="m-0">خصم 30%</p>
        </div></a><a class="offer hvr-float-shadow" href="#">
        <div class="offer-image"><img src="../../assets/images/landing/offer/offer-4.png" alt=""/></div>
        <div class="offer-body">
          <h4 class="mb-2">أدوات الحلاقة</h4>
          <p class="m-0">خصم 50%</p>
        </div></a><a class="offer hvr-float-shadow" href="#">
        <div class="offer-image"><img src="../../assets/images/landing/offer/offer-5.png" alt=""/></div>
        <div class="offer-body">
          <h4 class="mb-2">أدوات التجميل</h4>
          <p class="m-0">خصم 50%</p>
        </div></a>
    </div>
  </div>


@endsection
