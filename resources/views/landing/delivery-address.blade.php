@extends('landing.parent')

@section('Home-Title','Delivery Address')

@section('main-content')

<div class="main-content pb-5">
    <div class="cart-content">
      <div class="row w-100 m-0">
        <div class="col-md-8">
          <h4 class="mb-4">عنوان التوصيل</h4>
          <div class="row w-100 m-0">
            <div class="col-lg-6">
              <div class="cover-point-sec">
                <div class="form-check form-switch form-switch-md">
                  <input class="form-check-input" type="checkbox" id="switch8" role="switch"/>
                  <label class="form-check-label" for="switch8"></label><img class="bg-image-checked" src="../../assets/images/landing/active-cover-points.png" alt=""/><img class="bg-image" src="../../assets/images/landing/cover-points-box.png" alt=""/>
                  <div class="text">
                    <h2> العنوان الافتراضي</h2>
                    <div class="d-flex justify-content-start align-items-start">
                      <div class="shap">...</div>
                      <div>
                        <h3>التوصيل الى عنوانى الحالى</h3>
                        <h4>اول مكرم ، مدينة نصر   </h4>
                        <h4>القاهرة جمهورية مصر العربية</h4>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="cover-point-sec">
                <div class="form-check form-switch form-switch-md">
                  <input class="form-check-input" type="checkbox" id="switch8" role="switch"/>
                  <label class="form-check-label" for="switch8"></label><img class="bg-image-checked" src="../../assets/images/landing/active-cover-points.png" alt=""/><img class="bg-image" src="../../assets/images/landing/cover-points-box.png" alt=""/>
                  <div class="text">
                    <h2> العنوان الافتراضي</h2>
                    <div class="d-flex justify-content-start align-items-start">
                      <div class="shap">...</div>
                      <div>
                        <h3>التوصيل الى عنوانى الحالى</h3>
                        <h4>اول مكرم ، مدينة نصر   </h4>
                        <h4>القاهرة جمهورية مصر العربية</h4>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row justify-content-center">
            <div class="col-lg-12"><a class="load-more-sec mb-5">
                <p>اضافة عنوان جديد</p></a></div>
          </div>
          <div class="payments-list-check ways mt-5">
            <h4 class="mb-4">اختار طريقة الشحن</h4>
            <ul class="p-0 m-0">
              <li class="me-md-2">
                <input class="active" type="radio" name="payment" id="way-1">
                <label class="active" for="way-1">
                  <div class="title">طريقة شحن (1)</div>
                </label>
              </li>
              <li>
                <input type="radio" name="payment" id="way-2">
                <label for="way-2">
                  <div class="title">طريقة شحن (1)</div>
                </label>
              </li>
              <li class="me-md-2">
                <input type="radio" name="payment" id="way-3">
                <label for="way-3">
                  <div class="title">طريقة شحن (1)</div>
                </label>
              </li>
              <li>
                <input type="radio" name="payment" id="way-4">
                <label for="way-4">
                  <div class="title">طريقة شحن (1)</div>
                </label>
              </li>
            </ul>
          </div>
          <div class="day-time mt-5">
            <h4 class="mb-4">يوم ووقت التوصيل</h4>
            <div class="d-flex justify-content-between align-items-start">
              <button class="btn btnn hvr-shadow" data-bs-toggle="modal" data-bs-target="#dayModal">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#calender"></use>
                </svg><span>أختر يوم التوصيل</span>
                <div class="arrow">
                  <svg>
                    <use href="../../assets/images/icons/icons.svg#arrow-3"></use>
                  </svg>
                </div>
              </button>
              <button class="btn btnn hvr-shadow" data-bs-toggle="modal" data-bs-target="#timeModal">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#clock"></use>
                </svg><span>أختر وقت التوصيل</span>
                <div class="arrow">
                  <svg>
                    <use href="../../assets/images/icons/icons.svg#arrow-3"></use>
                  </svg>
                </div>
              </button>
            </div>
          </div>
          <div class="order mt-5">
            <h4 class="mb-4">ملخص الطلبية</h4>
            <div class="products mt-3">
              <div class="product d-flex justify-content-between mb-3">
                <div class="img"><img src="../../assets/images/products/product-1.png" alt=""/></div>
                <div>
                  <p class="name m-0">اسم التاجر</p>
                  <h4>جبنة لافاش كيري</h4><span class="number">48 قطعة</span>
                </div>
                <div>
                  <p class="price m-0">150 دينار</p><span class="old-price m-0">50 دينار</span>
                </div>
                <p class="amount m-0">الكمية 1</p>
              </div>
              <div class="product d-flex justify-content-between mb-3">
                <div class="img"><img src="../../assets/images/products/product-1.png" alt=""/></div>
                <div>
                  <p class="name m-0">اسم التاجر</p>
                  <h4>جبنة لافاش كيري</h4><span class="number">48 قطعة</span>
                </div>
                <div>
                  <p class="price m-0">150 دينار</p><span class="old-price m-0">50 دينار</span>
                </div>
                <p class="amount m-0">الكمية 1</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="order-details delivery">
            <div class="p-4">
              <h4 class="mb-4">طريقة الدفع</h4>
              <ul class="payments-list-check">
                <li>
                  <input type="radio" name="payment" id="visa">
                  <label for="visa">
                    <div class="title">فيزا / ماستر كارد</div><img class="img" src="../../assets/images/payments/visa.png" alt=""><img class="img-active" src="../../assets/images/payments/visa-active.png" alt="">
                  </label>
                </li>
                <li>
                  <input type="radio" name="payment" id="apple">
                  <label for="apple">
                    <div class="title">أبل باي</div><img class="img" src="../../assets/images/payments/apple-p.png" alt=""><img class="img-active" src="../../assets/images/payments/apple-p-active.png" alt="">
                  </label>
                </li>
                <li>
                  <input type="radio" name="payment" id="mada">
                  <label for="mada">
                    <div class="title">كي نت / مدى</div><img class="img" src="../../assets/images/payments/mada.png" alt=""><img class="img-active" src="../../assets/images/payments/mada-active.png" alt="">
                  </label>
                </li>
                <li>
                  <input type="radio" name="payment" id="cash">
                  <label for="cash">
                    <div class="title">كاش</div><img class="img" src="../../assets/images/payments/cash.png" alt=""><img class="img-active" src="../../assets/images/payments/cash-active.png" alt="">
                  </label>
                </li>
              </ul>
              <hr>
              <div class="order-total">
                <ul>
                  <li>
                    <h5>السعر الفرعي التقريبي </h5>
                    <h6>350 شيكل</h6>
                  </li>
                  <li>
                    <h5>التوصيل<span class="express">Express </span></h5>
                    <h6>350 شيكل</h6>
                  </li>
                  <li>
                    <h5>كوبون الخصم </h5>
                    <h6>20 شيكل</h6>
                  </li>
                  <li>
                    <h5>إجمالي الطلبات</h5>
                    <h6>620 شيكل</h6>
                  </li>
                </ul>
              </div>
              <div class="comp-order mt-4"><a class="btnn-orange btn btnn hvr-shadow btn-fill px-5" href="{{route('check-out-order')}}"><span>اتمام الطلب </span></a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="text-center mt-5">
      <h3>لنا مكان فى كل دُ كان</h3>
      <p>جميع عمليات الدفع والطلب لدينا مضمونة 100%     </p>
    </div>
    <div class="modal fade" id="dayModal" tabindex="-1" aria-labelledby="dayModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content time-modal">
          <div class="modal-header">
            <h5 class="modal-title" id="timeModalLabel"> </h5>
            <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body p-5">
            <div class="cansel-offer-modal p-0"> <span class="close" data-bs-toggle="modal" data-bs-target="#dayModal">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#close"></use>
                </svg></span>
              <h5 class="fw-bold">معاد التوصيل</h5>
              <ul class="p-0 m-0 w-100 payments-list-check mt-4">
                <li class="me-md-2">
                  <input class="active" type="radio" name="payment" id="day-1">
                  <label class="active" for="day-1">
                    <div class="title">اليوم 03/03/2022</div>
                    <div class="express">
                      <p class="m-0 fw-bold">Express</p><span>SR 20+</span><img class="img" src="../../assets/images/landing/express.svg" alt="">
                    </div>
                  </label>
                </li>
                <li>
                  <input type="radio" name="payment" id="day-2">
                  <label for="day-2">
                    <div class="title">غدا 22/4/2021</div>
                  </label>
                </li>
                <li class="me-md-2">
                  <input type="radio" name="payment" id="day-3">
                  <label for="day-3">
                    <div class="title">الثلاثاء 22/4/2021</div>
                  </label>
                </li>
                <li>
                  <input type="radio" name="payment" id="day-4">
                  <label for="day-4">
                    <div class="title">الأربعاء 22/4/2021</div>
                  </label>
                </li>
              </ul>
              <button class="w-100 btnn-orange btn-fill btn btnn hvr-shadow" data-bs-toggle="modal" data-bs-target="#dayModal"><span>تم </span></button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="timeModal" tabindex="-1" aria-labelledby="timeModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content time-modal">
          <div class="modal-header">
            <h5 class="modal-title" id="timeModalLabel"> </h5>
            <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body p-5">
            <div class="cansel-offer-modal p-0"> <span class="close" data-bs-toggle="modal" data-bs-target="#timeModal">
                <svg>
                  <use href="../../assets/images/icons/icons.svg#close"></use>
                </svg></span>
              <h5 class="fw-bold">وقت التوصيل</h5>
              <ul class="p-0 m-0 w-100 payments-list-check mt-4">
                <li class="me-md-2">
                  <input class="active" type="radio" name="payment" id="time-1">
                  <label class="active" for="time-1">
                    <div class="title">10-12 صباحاَ</div>
                  </label>
                </li>
                <li>
                  <input type="radio" name="payment" id="time-2">
                  <label for="time-2">
                    <div class="title">10-12 صباحاَ</div>
                  </label>
                </li>
                <li class="me-md-2">
                  <input type="radio" name="payment" id="time-3">
                  <label for="time-3">
                    <div class="title">10-12 صباحاَ </div>
                  </label>
                </li>
                <li>
                  <input type="radio" name="payment" id="time-4">
                  <label for="time-4">
                    <div class="title">10-12 صباحاَ</div>
                  </label>
                </li>
                <li>
                  <input type="radio" name="payment" id="time-4">
                  <label for="time-4">
                    <div class="title">10-12 صباحاَ</div>
                  </label>
                </li>
              </ul>
              <button class="w-100 btnn-orange btn-fill btn btnn hvr-shadow" data-bs-toggle="modal" data-bs-target="#timeModal"><span>تم               </span></button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
