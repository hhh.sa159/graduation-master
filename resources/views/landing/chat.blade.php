@extends('landing.parent')

@section('Home-Title','Chat')

@section('add-deal')

<div class="landing-notis-head d-flex justify-content-center align-items-center">
    <p class="m-0">هل تريد ان تبدء صفقة جديدة الان ؟ </p><a class="btnn-orange btn btnn hvr-shadow btn-fill" href="{{route('add-deal')}}"><span>اضافة صفقة</span></a>
  </div>

@endsection

@section('main-content')

<div class="main-content pb-5">
    <div class="page-chat">
      <div class="chat-users">
        <div class="nav-tabs-1">
          <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
              <button class="nav-link active" id="All-messages-tab" data-bs-toggle="tab" data-bs-target="#All-messages" type="button" role="tab" aria-controls="All-messages" aria-selected="true">الكل</button>
            </li>
            <li class="nav-item" role="presentation">
              <button class="nav-link" id="Stories-messages-tab" data-bs-toggle="tab" data-bs-target="#Stories-messages" type="button" role="tab" aria-controls="Stories-messages" aria-selected="false">استفسارات القصص</button>
            </li>
            <li class="nav-item" role="presentation">
              <button class="nav-link" id="Deal-Messages-tab" data-bs-toggle="tab" data-bs-target="#Deal-Messages" type="button" role="tab" aria-controls="Deal-Messages" aria-selected="false">استفسارات اﻟﺼﻔﻘﺎت</button>
            </li>
          </ul>
          <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="All-messages" role="tabpanel" aria-labelledby="All-messages-tab">
              <div class="chat-users-list">
                <ul>
                  <li>
                    <div class="user-details">
                      <div class="img"> <img src="../../assets/images/photo-top.png" alt=""></div>
                      <div class="name-message">
                        <h3> Mohamed ahmed</h3>
                        <div class="new-message"><span> ﻣﺘﺎح ﻋﻨﺪك 5 ﻛﻴﻠﻮ ؟                                 </span></div>
                      </div>
                    </div>
                    <div class="message-time"> <span>اﻟﻴﻮم 40 ،:12 ﻣﺴﺎء </span></div>
                  </li>
                  <li>
                    <div class="user-details">
                      <div class="img"> <img src="../../assets/images/photo-top.png" alt=""></div>
                      <div class="name-message">
                        <h3> Mohamed ahmed</h3>
                        <div class="new-message"><span> ﻣﺘﺎح ﻋﻨﺪك 5 ﻛﻴﻠﻮ ؟                                 </span></div>
                      </div>
                    </div>
                    <div class="message-time"> <span>اﻟﻴﻮم 40 ،:12 ﻣﺴﺎء </span></div>
                  </li>
                  <li>
                    <div class="user-details">
                      <div class="img"> <img src="../../assets/images/photo-top.png" alt=""></div>
                      <div class="name-message">
                        <h3> Mohamed ahmed</h3>
                        <div class="new-message"><span> ﻣﺘﺎح ﻋﻨﺪك 5 ﻛﻴﻠﻮ ؟                                 </span></div>
                      </div>
                    </div>
                    <div class="message-time"> <span>اﻟﻴﻮم 40 ،:12 ﻣﺴﺎء </span></div>
                  </li>
                  <li>
                    <div class="user-details">
                      <div class="img"> <img src="../../assets/images/photo-top.png" alt=""></div>
                      <div class="name-message">
                        <h3> Mohamed ahmed</h3>
                        <div class="new-message"><span> ﻣﺘﺎح ﻋﻨﺪك 5 ﻛﻴﻠﻮ ؟                                 </span></div>
                      </div>
                    </div>
                    <div class="message-time"> <span>اﻟﻴﻮم 40 ،:12 ﻣﺴﺎء </span></div>
                  </li>
                </ul>
              </div>
            </div>
            <div class="tab-pane fade" id="Stories-messages" role="tabpanel" aria-labelledby="Stories-messages-tab">
              <div class="chat-users-list">
                <ul>
                  <li>
                    <div class="user-details">
                      <div class="img"> <img src="../../assets/images/photo-top.png" alt=""></div>
                      <div class="name-message">
                        <h3> Mohamed ahmed</h3>
                        <div class="new-message"><span> ﻣﺘﺎح ﻋﻨﺪك 5 ﻛﻴﻠﻮ ؟                                 </span></div>
                      </div>
                    </div>
                    <div class="message-time"> <span>اﻟﻴﻮم 40 ،:12 ﻣﺴﺎء </span></div>
                  </li>
                  <li>
                    <div class="user-details">
                      <div class="img"> <img src="../../assets/images/photo-top.png" alt=""></div>
                      <div class="name-message">
                        <h3> Mohamed ahmed</h3>
                        <div class="new-message"><span> ﻣﺘﺎح ﻋﻨﺪك 5 ﻛﻴﻠﻮ ؟                                 </span></div>
                      </div>
                    </div>
                    <div class="message-time"> <span>اﻟﻴﻮم 40 ،:12 ﻣﺴﺎء </span></div>
                  </li>
                  <li>
                    <div class="user-details">
                      <div class="img"> <img src="../../assets/images/photo-top.png" alt=""></div>
                      <div class="name-message">
                        <h3> Mohamed ahmed</h3>
                        <div class="new-message"><span> ﻣﺘﺎح ﻋﻨﺪك 5 ﻛﻴﻠﻮ ؟                                 </span></div>
                      </div>
                    </div>
                    <div class="message-time"> <span>اﻟﻴﻮم 40 ،:12 ﻣﺴﺎء </span></div>
                  </li>
                  <li>
                    <div class="user-details">
                      <div class="img"> <img src="../../assets/images/photo-top.png" alt=""></div>
                      <div class="name-message">
                        <h3> Mohamed ahmed</h3>
                        <div class="new-message"><span> ﻣﺘﺎح ﻋﻨﺪك 5 ﻛﻴﻠﻮ ؟                                 </span></div>
                      </div>
                    </div>
                    <div class="message-time"> <span>اﻟﻴﻮم 40 ،:12 ﻣﺴﺎء </span></div>
                  </li>
                </ul>
              </div>
            </div>
            <div class="tab-pane fade" id="Deal-Messages" role="tabpanel" aria-labelledby="Deal-Messages-tab"> </div>
          </div>
        </div>
      </div>
      <div class="message-view-box">
        <div class="message-view-box-head">
          <div class="user-details">
            <div class="img"> <img src="../../assets/images/photo-top.png" alt=""></div>
            <div class="name-message">
              <h3> Mohamed ahmed</h3>
              <div class="new-message"><span> اليوم ، 40:12 مساء</span></div>
            </div>
          </div>
          <div class="message-view-box-actions">
            <button class="btnn-orange btn btnn mr-2 hvr-shadow" type="button">
              <svg>
                <use href="../../assets/images/icons/icons.svg#phone"></use>
              </svg>
            </button>
            <button class="btnn-orange btn btnn mr-2 hvr-shadow" type="button">
              <svg>
                <use href="../../assets/images/icons/icons.svg#envelope"></use>
              </svg>
            </button>
          </div>
        </div>
        <div class="message-view-box-body">
          <ul class="list-messages">
            <li class="current-user">
              <div class="message-box">
                <div class="message-item">كم الكمية المتاحة معك</div>
                <div class="read-check">
                  <svg>
                    <use href="../../assets/images/icons/icons.svg#read-check"></use>
                  </svg>
                </div>
              </div>
            </li>
            <li class="other-user">
              <div class="user-avatar"><img src="../../assets/images/photo-top.png" alt=""></div>
              <div class="message-box">
                <div class="message-item">نعم هذا المنتج متاح عندى </div>
                <div class="message-read">منذ ساعتان</div>
              </div>
            </li>
            <li class="current-user">
              <div class="message-box">
                <div class="message-item">كم الكمية المتاحة معك</div>
                <div class="read-check">
                  <svg>
                    <use href="../../assets/images/icons/icons.svg#read-check"></use>
                  </svg>
                </div>
              </div>
            </li>
            <li class="other-user">
              <div class="user-avatar"><img src="../../assets/images/photo-top.png" alt=""></div>
              <div class="message-box">
                <div class="message-item">نعم هذا المنتج متاح عندى </div>
                <div class="message-read">منذ ساعتان</div>
              </div>
            </li>
            <li class="current-user">
              <div class="message-box">
                <div class="message-item">كم الكمية المتاحة معك</div>
                <div class="read-check">
                  <svg>
                    <use href="../../assets/images/icons/icons.svg#read-check"></use>
                  </svg>
                </div>
              </div>
            </li>
            <li class="other-user">
              <div class="user-avatar"><img src="../../assets/images/photo-top.png" alt=""></div>
              <div class="message-box">
                <div class="message-item">نعم هذا المنتج متاح عندى </div>
                <div class="message-read">منذ ساعتان</div>
              </div>
            </li>
            <li class="current-user">
              <div class="message-box">
                <div class="message-item">كم الكمية المتاحة معك</div>
                <div class="read-check">
                  <svg>
                    <use href="../../assets/images/icons/icons.svg#read-check"></use>
                  </svg>
                </div>
              </div>
            </li>
            <li class="other-user">
              <div class="user-avatar"><img src="../../assets/images/photo-top.png" alt=""></div>
              <div class="message-box">
                <div class="message-item">نعم هذا المنتج متاح عندى </div>
                <div class="message-read">منذ ساعتان            .message-view-box-bottom</div>
              </div>
            </li>
          </ul>
          <form action="">
            <textarea name="" placeholder="أكتب رسالتك"></textarea>
            <button class="btnn-orange btn btnn mr-2 btn-fill hvr-shadow" type="button">
              <svg>
                <use href="../../assets/images/icons/icons.svg#send"></use>
              </svg>
            </button>
          </form>
        </div>
      </div>
    </div>
  </div>

@endsection

