@extends('landing.parent')

@section('Home-Title','Orders')

@section('add-deal')

<div class="landing-notis-head d-flex justify-content-center align-items-center">
    <p class="m-0">هل تريد ان تبدء صفقة جديدة الان ؟ </p><a class="btnn-orange btn btnn hvr-shadow btn-fill" href="{{route('add-deal')}}"><span>اضافة صفقة</span></a>
  </div>

@endsection

@section('main-content')

<div class="main-content pb-5">
    <div class="header">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{route('home')}}">الرئيسية</a></li>
          <li class="breadcrumb-item">الطلبات</li>
        </ol>
      </nav>
    </div>
    <div class="bargains-content mt-5">
      <div class="dash-tabs">
        <div class="nav-tabs-1">
          <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
              <button class="nav-link active" id="Voucher-data-tab" data-bs-toggle="tab" data-bs-target="#Voucher-data" type="button" role="tab" aria-controls="Voucher-data" aria-selected="true">طلباتى الحالية</button>
            </li>
            <li class="nav-item" role="presentation">
              <button class="nav-link" id="Voucher-Included-tab" data-bs-toggle="tab" data-bs-target="#Voucher-Included" type="button" role="tab" aria-controls="Voucher-Included" aria-selected="false">طلباتى السابقة </button>
            </li>
            <li class="nav-item" role="presentation">
              <button class="nav-link" id="excluded-coupon-tab" data-bs-toggle="tab" data-bs-target="#excluded-coupon" type="button" role="tab" aria-controls="excluded-coupon" aria-selected="false">المرفوضة</button>
            </li>
          </ul>
          <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="Voucher-data" role="tabpanel" aria-labelledby="Voucher-data-tab">
              <div class="all-bargins">
                <div class="row m-0 w-100 mt-4">
                  <div class="col-md-4 mb-4"><a class="bargain hvr-float-shadow" href="{{route('order-details')}}">
                      <div class="bargain-head">
                        <h6 class="m-0">حالة الطلب</h6><span> يتم التوصيل </span>
                      </div>
                      <div class="bargain-body">
                        <div class="row w-100 m-0">
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>إجمالي الطلب</span>
                              <h6>230 شيكل</h6>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>التاجر</span>
                              <h6>Grocery shope</h6>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>وقت وتاريخ الطلب</span>
                              <h6>20/10/2021 - 10:50 م</h6>
                            </div>
                          </div>
                        </div>
                      </div></a>
                  </div>
                  <div class="col-md-4 mb-4"><a class="bargain hvr-float-shadow" href="{{route('order-details')}}">
                      <div class="bargain-head">
                        <h6 class="m-0">حالة الطلب</h6><span> يتم التوصيل </span>
                      </div>
                      <div class="bargain-body">
                        <div class="row w-100 m-0">
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>إجمالي الطلب</span>
                              <h6>230 شيكل</h6>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>التاجر</span>
                              <h6>Grocery shope</h6>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>وقت وتاريخ الطلب</span>
                              <h6>20/10/2021 - 10:50 م</h6>
                            </div>
                          </div>
                        </div>
                      </div></a>
                  </div>
                  <div class="col-md-4 mb-4"><a class="bargain hvr-float-shadow" href="{{route('order-details')}}">
                      <div class="bargain-head">
                        <h6 class="m-0">حالة الطلب</h6><span> يتم التوصيل </span>
                      </div>
                      <div class="bargain-body">
                        <div class="row w-100 m-0">
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>إجمالي الطلب</span>
                              <h6>230 شيكل</h6>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>التاجر</span>
                              <h6>Grocery shope</h6>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>وقت وتاريخ الطلب</span>
                              <h6>20/10/2021 - 10:50 م</h6>
                            </div>
                          </div>
                        </div>
                      </div></a>
                  </div>
                  <div class="col-md-4 mb-4"><a class="bargain hvr-float-shadow" href="{{route('order-details')}}">
                      <div class="bargain-head">
                        <h6 class="m-0">حالة الطلب</h6><span> يتم التوصيل </span>
                      </div>
                      <div class="bargain-body">
                        <div class="row w-100 m-0">
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>إجمالي الطلب</span>
                              <h6>230 شيكل</h6>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>التاجر</span>
                              <h6>Grocery shope</h6>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>وقت وتاريخ الطلب</span>
                              <h6>20/10/2021 - 10:50 م</h6>
                            </div>
                          </div>
                        </div>
                      </div></a>
                  </div>
                  <div class="col-md-4 mb-4"><a class="bargain hvr-float-shadow" href="{{route('order-details')}}">
                      <div class="bargain-head">
                        <h6 class="m-0">حالة الطلب</h6><span> يتم التوصيل </span>
                      </div>
                      <div class="bargain-body">
                        <div class="row w-100 m-0">
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>إجمالي الطلب</span>
                              <h6>230 شيكل</h6>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>التاجر</span>
                              <h6>Grocery shope</h6>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>وقت وتاريخ الطلب</span>
                              <h6>20/10/2021 - 10:50 م</h6>
                            </div>
                          </div>
                        </div>
                      </div></a>
                  </div>
                  <div class="col-md-4 mb-4"><a class="bargain hvr-float-shadow" href="{{route('order-details')}}">
                      <div class="bargain-head">
                        <h6 class="m-0">حالة الطلب</h6><span> يتم التوصيل </span>
                      </div>
                      <div class="bargain-body">
                        <div class="row w-100 m-0">
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>إجمالي الطلب</span>
                              <h6>230 شيكل</h6>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>التاجر</span>
                              <h6>Grocery shope</h6>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>وقت وتاريخ الطلب</span>
                              <h6>20/10/2021 - 10:50 م</h6>
                            </div>
                          </div>
                        </div>
                      </div></a>
                  </div>
                  <div class="col-md-4 mb-4"><a class="bargain hvr-float-shadow" href="{{route('order-details')}}">
                      <div class="bargain-head">
                        <h6 class="m-0">حالة الطلب</h6><span> يتم التوصيل </span>
                      </div>
                      <div class="bargain-body">
                        <div class="row w-100 m-0">
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>إجمالي الطلب</span>
                              <h6>230 شيكل</h6>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>التاجر</span>
                              <h6>Grocery shope</h6>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>وقت وتاريخ الطلب</span>
                              <h6>20/10/2021 - 10:50 م</h6>
                            </div>
                          </div>
                        </div>
                      </div></a>
                  </div>
                  <div class="col-md-4 mb-4"><a class="bargain hvr-float-shadow" href="{{route('order-details')}}">
                      <div class="bargain-head">
                        <h6 class="m-0">حالة الطلب</h6><span> يتم التوصيل </span>
                      </div>
                      <div class="bargain-body">
                        <div class="row w-100 m-0">
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>إجمالي الطلب</span>
                              <h6>230 شيكل</h6>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>التاجر</span>
                              <h6>Grocery shope</h6>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>وقت وتاريخ الطلب</span>
                              <h6>20/10/2021 - 10:50 م</h6>
                            </div>
                          </div>
                        </div>
                      </div></a>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="Voucher-Included" role="tabpanel" aria-labelledby="Voucher-Included-tab">
              <div class="all-bargins">
                <div class="row m-0 w-100 mt-4">
                  <div class="col-md-4 mb-4"><a class="bargain hvr-float-shadow" href="{{route('order-details')}}">
                      <div class="bargain-head">
                        <h6 class="m-0">حالة الطلب</h6><span> يتم التوصيل </span>
                      </div>
                      <div class="bargain-body">
                        <div class="row w-100 m-0">
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>إجمالي الطلب</span>
                              <h6>230 شيكل</h6>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>التاجر</span>
                              <h6>Grocery shope</h6>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>وقت وتاريخ الطلب</span>
                              <h6>20/10/2021 - 10:50 م</h6>
                            </div>
                          </div>
                        </div>
                      </div></a>
                  </div>
                  <div class="col-md-4 mb-4"><a class="bargain hvr-float-shadow" href="{{route('order-details')}}">
                      <div class="bargain-head">
                        <h6 class="m-0">حالة الطلب</h6><span> يتم التوصيل </span>
                      </div>
                      <div class="bargain-body">
                        <div class="row w-100 m-0">
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>إجمالي الطلب</span>
                              <h6>230 شيكل</h6>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>التاجر</span>
                              <h6>Grocery shope</h6>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>وقت وتاريخ الطلب</span>
                              <h6>20/10/2021 - 10:50 م</h6>
                            </div>
                          </div>
                        </div>
                      </div></a>
                  </div>
                  <div class="col-md-4 mb-4"><a class="bargain hvr-float-shadow" href="{{route('order-details')}}">
                      <div class="bargain-head">
                        <h6 class="m-0">حالة الطلب</h6><span> يتم التوصيل </span>
                      </div>
                      <div class="bargain-body">
                        <div class="row w-100 m-0">
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>إجمالي الطلب</span>
                              <h6>230 شيكل</h6>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>التاجر</span>
                              <h6>Grocery shope</h6>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>وقت وتاريخ الطلب</span>
                              <h6>20/10/2021 - 10:50 م</h6>
                            </div>
                          </div>
                        </div>
                      </div></a>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="excluded-coupon" role="tabpanel" aria-labelledby="excluded-coupon-tab">
              <div class="all-bargins">
                <div class="row m-0 w-100 mt-4">
                  <div class="col-md-4 mb-4"><a class="bargain hvr-float-shadow" href="{{route('order-details')}}">
                      <div class="bargain-head">
                        <h6 class="m-0">حالة الطلب</h6><span> يتم التوصيل </span>
                      </div>
                      <div class="bargain-body">
                        <div class="row w-100 m-0">
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>إجمالي الطلب</span>
                              <h6>230 شيكل</h6>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>التاجر</span>
                              <h6>Grocery shope</h6>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>وقت وتاريخ الطلب</span>
                              <h6>20/10/2021 - 10:50 م</h6>
                            </div>
                          </div>
                        </div>
                      </div></a>
                  </div>
                  <div class="col-md-4 mb-4"><a class="bargain hvr-float-shadow" href="{{route('order-details')}}">
                      <div class="bargain-head">
                        <h6 class="m-0">حالة الطلب</h6><span> يتم التوصيل </span>
                      </div>
                      <div class="bargain-body">
                        <div class="row w-100 m-0">
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>إجمالي الطلب</span>
                              <h6>230 شيكل</h6>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>التاجر</span>
                              <h6>Grocery shope</h6>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>وقت وتاريخ الطلب</span>
                              <h6>20/10/2021 - 10:50 م</h6>
                            </div>
                          </div>
                        </div>
                      </div></a>
                  </div>
                  <div class="col-md-4 mb-4"><a class="bargain hvr-float-shadow" href="{{route('order-details')}}">
                      <div class="bargain-head">
                        <h6 class="m-0">حالة الطلب</h6><span> يتم التوصيل </span>
                      </div>
                      <div class="bargain-body">
                        <div class="row w-100 m-0">
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>إجمالي الطلب</span>
                              <h6>230 شيكل</h6>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>التاجر</span>
                              <h6>Grocery shope</h6>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>وقت وتاريخ الطلب</span>
                              <h6>20/10/2021 - 10:50 م</h6>
                            </div>
                          </div>
                        </div>
                      </div></a>
                  </div>
                  <div class="col-md-4 mb-4"><a class="bargain hvr-float-shadow" href="{{route('order-details')}}">
                      <div class="bargain-head">
                        <h6 class="m-0">حالة الطلب</h6><span> يتم التوصيل </span>
                      </div>
                      <div class="bargain-body">
                        <div class="row w-100 m-0">
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>إجمالي الطلب</span>
                              <h6>230 شيكل</h6>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>التاجر</span>
                              <h6>Grocery shope</h6>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>وقت وتاريخ الطلب</span>
                              <h6>20/10/2021 - 10:50 م</h6>
                            </div>
                          </div>
                        </div>
                      </div></a>
                  </div>
                  <div class="col-md-4 mb-4"><a class="bargain hvr-float-shadow" href="{{route('order-details')}}">
                      <div class="bargain-head">
                        <h6 class="m-0">حالة الطلب</h6><span> يتم التوصيل </span>
                      </div>
                      <div class="bargain-body">
                        <div class="row w-100 m-0">
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>إجمالي الطلب</span>
                              <h6>230 شيكل</h6>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>التاجر</span>
                              <h6>Grocery shope</h6>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="bargain-body-sec"><span>وقت وتاريخ الطلب</span>
                              <h6>20/10/2021 - 10:50 م</h6>
                            </div>
                          </div>
                        </div>
                      </div></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection

