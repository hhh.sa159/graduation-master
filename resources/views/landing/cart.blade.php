@extends('landing.parent')

@section('Home-Title','Cart')

@section('add-deal')

<div class="landing-notis-head d-flex justify-content-center align-items-center">
    <p class="m-0">هل تريد ان تبدء صفقة جديدة الان ؟ </p><a class="btnn-orange btn btnn hvr-shadow btn-fill" href="{{route('add-deal')}}"><span>اضافة صفقة</span></a>
  </div>

@endsection

@section('main-content')

<div class="main-content pb-5">
    <div class="cart-content">
      <div class="row w-100 m-0">
        <div class="col-md-8">
          <div class="header d-md-flex justify-content-md-between align-items-md-center">
            <div class="d-md-flex justify-content-md-between align-items-md-center">
              <h4 class="m-0">المنتجات</h4><span class="mx-md-3 me-3">3 منتجات من</span><a href="#">Grocery shope</a>
            </div><a class="back" href="#">عودة</a>
          </div>
          <div class="products mt-3">
            <div class="product d-flex justify-content-between mb-3">
              <div class="img"><img src="../../assets/images/products/product-1.png" alt=""/></div>
              <div>
                <p class="name m-0">اسم التاجر</p>
                <h4>جبنة لافاش كيري</h4><span class="number">48 قطعة</span>
              </div>
              <div>
                <p class="price m-0">150 دينار</p><span class="old-price m-0">50 دينار</span>
              </div>
              <div class="input-number">
                <div class="form-group">
                  <div class="input-number-control"><a class="input-increment" href="#"> +</a>
                    <input type="text" id="input-450" placeholder="0" value="0"/><a class="input-decrement" href="#"> -</a>
                  </div>
                </div>
              </div>
              <svg>
                <use href="../../assets/images/icons/icons.svg#delete"> </use>
              </svg>
            </div>
            <div class="product d-flex justify-content-between mb-3">
              <div class="img"><img src="../../assets/images/products/product-1.png" alt=""/></div>
              <div>
                <p class="name m-0">اسم التاجر</p>
                <h4>جبنة لافاش كيري</h4><span class="number">48 قطعة</span>
              </div>
              <div>
                <p class="price m-0">150 دينار</p><span class="old-price m-0">50 دينار</span>
              </div>
              <div class="input-number">
                <div class="form-group">
                  <div class="input-number-control"><a class="input-increment" href="#"> +</a>
                    <input type="text" id="input-475" placeholder="0" value="0"/><a class="input-decrement" href="#"> -</a>
                  </div>
                </div>
              </div>
              <svg>
                <use href="../../assets/images/icons/icons.svg#delete"> </use>
              </svg>
            </div>
            <div class="product d-flex justify-content-between mb-3">
              <div class="img"><img src="../../assets/images/products/product-1.png" alt=""/></div>
              <div>
                <p class="name m-0">اسم التاجر</p>
                <h4>جبنة لافاش كيري</h4><span class="number">48 قطعة</span>
              </div>
              <div>
                <p class="price m-0">150 دينار</p><span class="old-price m-0">50 دينار</span>
              </div>
              <div class="input-number">
                <div class="form-group">
                  <div class="input-number-control"><a class="input-increment" href="#"> +</a>
                    <input type="text" id="input-775" placeholder="0" value="0"/><a class="input-decrement" href="#"> -</a>
                  </div>
                </div>
              </div>
              <svg>
                <use href="../../assets/images/icons/icons.svg#delete"> </use>
              </svg>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="order-details">
            <h4 class="p-4">تفاصيل الطلب</h4>
            <div class="coupon-note">
              <div class="icon"> <img src="../../assets/images/landing/coupon-icon.png" alt=""></div>
              <h3 class="m-0">لديك كوبون خصم ؟</h3>
              <div class="coupon-finish"> <img src="../../assets/images/landing/coupon-finish.png" alt=""></div>
            </div>
            <div class="p-4">
              <div class="code-input d-md-flex justify-content-md-between align-md-items-center">
                <div class="form-group">
                  <input class="form-control" type="text" id="input-25" placeholder="أكتب كوبون الخصم"/>
                </div>
                <button class="btnn-orange btn btnn btn-fill px-5 hvr-shadow" type="button"><span>تفعيل</span>
                </button>
              </div>
              <hr>
              <div class="d-flex justify-content-between align-items-center"><span>المجموع</span><span class="fw-bold">350 شيكل</span></div>
              <div class="comp-order mt-4"><a class="btnn-orange btn btnn hvr-shadow btn-fill px-5" href="../../html/auth-landing/login.html"><span>اتمام الطلب </span></a></div>
            </div>
          </div>
        </div>
      </div>
      <div class="text-center mt-5">
        <h3>لنا مكان فى كل دُ كان</h3>
        <p>جميع عمليات الدفع والطلب لدينا مضمونة 100%          </p>
      </div>
    </div>
  </div>

@endsection
