@extends('cmsAdmin.parent')

@section('title','Update Seller')

@section('big-title','Update Seller')

@section('main-page','Home')

@section('sub-page','Sellers')

@section('content')

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Update Seller</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form id="update-form">
                  @csrf

                <div class="card-body">
                  <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name" placeholder="Enter Name"
                    value="{{$seller->name}}">
                  </div>
                  <div class="form-group">
                    <label for="name">Email</label>
                    <input type="email" class="form-control" id="email" placeholder="Enter Email"
                    value="{{$seller->email}}">
                  </div>
                  <div class="form-group">
                    <label for="email">Phone</label>
                    <input type="number" class="form-control" id="mobile_number" placeholder="Enter Phone"
                    value="{{$seller->email}}">
                  </div>
                  {{-- <div class="form-group">
                    <div class="custom-control custom-switch">
                      <input type="checkbox" class="custom-control-input" id="active"
                      @if($admin->active) checked @endif>
                      <label class="custom-control-label" for="active">Active</label>
                    </div>
                  </div> --}}
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="button" onclick="update({{$seller->id}})" class="btn btn-primary">Update</button>
                </div>
              </form>
            </div>
            <!-- /.card -->


    </section>
    <!-- /.content -->

@endsection

@section('scripts')
    <script>

        function update(id){
            axios.put('/admin/sellers/'+id,{
                name:document.getElementById('name').value,
                email:document.getElementById('email').value,
                mobile_number:document.getElementById('mobile_number').value

            })
            .then(function (response) {
                // handle success
                console.log(response);
                toastr.success(response.data.message);
                window.location.href = "/admin/sellers";
            })
            .catch(function (error) {
                // handle error
                console.log(error);
                toastr.error(error.response.data.message);
            })
            .then(function () {
                // always executed
            });
        }
    </script>
@endsection


