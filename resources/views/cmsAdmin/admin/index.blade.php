@extends('cmsAdmin.parent')

@section('title','admins')

@section('big-title','admins')

@section('main-page','Home')

@section('sub-page','admins')

@section('content')

<section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Admin Information</h3>

              <div class="card-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                  <div class="input-group-append">
                    <button type="submit" class="btn btn-default">
                      <i class="fas fa-search"></i>
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
              <table class="table table-hover table-bordered table-striped text-nowrap">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Status</th>
                    <th>Created At</th>
                    <th>Updated At</th>
                    <th>Setting</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($admins as $admin )
                    <tr>
                        <td>{{$admin->id}}</td>
                        <td>{{$admin->name}}</td>
                        <td>{{$admin->email}}</td>
                        <td>{{$admin->created_at}}</td>
                        <td>{{$admin->updated_at}}</td>
                        <td>
                            <div class="btn-group">
                                <a href="{{route('admins.edit',$admin->id)}}" class="btn btn-info">
                                    <i class="far fa-edit"></i>
                                </a>
                                <a href="#" class="btn btn-danger"
                                onclick="confirmDestroy({{$admin->id}},this)">
                                    <i class="fas fa-trash"></i>
                                </a>
                              </div>
                        </td>
                        {{-- <td><span class="tag tag-success">Approved</span></td> --}}
                    </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
    </div><!-- /.container-fluid -->
</section>

@endsection

@section('scripts')

    <script>
        function confirmDestroy(id,reference){
            // alert("CONFIRM DESTROY FUNCTION");
            // console.log("CONFIRM DESTROY");
            // console.log(id);
            Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
            if (result.isConfirmed) {
                destroy(id,reference);
            }
            })
        }
        function destroy(id,reference){
            axios.delete('/admin/admins/'+id)
            .then(function (response) {
                // handle success
                console.log(response);
                reference.closest('tr').remove();
                showMessage(response.data);
            })
            .catch(function (error) {
                // handle error
                console.log(error);
                showMessage(error.response.data);
            })
            .then(function () {
                // always executed
            });
        }
        function showMessage(data){
            Swal.fire({
                icon: data.icon,
                title: data.title,
                text:data.text,
                showConfirmButton: false,
                timer: 1500
            })
        }
    </script>

@endsection
