@extends('cmsAdmin.parent')

@section('title','Create User')

@section('big-title','Create User')

@section('main-page','Home')

@section('sub-page','User')

@section('content')

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create User</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form id="create-form">
                  @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name" placeholder="Enter Name">
                  </div>
                  <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" placeholder="Enter Email">
                  </div>
                  <div class="form-group">
                    <label for="email">Phone</label>
                    <input type="number" class="form-control" id="mobile_number" placeholder="Enter Phone">
                  </div>
                  {{-- <div class="form-group">
                    <div class="custom-control custom-switch">
                      <input type="checkbox" class="custom-control-input" id="active">
                      <label class="custom-control-label" for="active">Active</label>
                    </div>
                  </div> --}}
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="button" onclick="store()" class="btn btn-primary">Store</button>
                </div>
              </form>
            </div>
            <!-- /.card -->


    </section>
    <!-- /.content -->

@endsection

@section('scripts')
    <script>

        function store(){
            axios.post('/admin/users',{
                name:document.getElementById('name').value,
                email:document.getElementById('email').value,
                mobile_number:document.getElementById('mobile_number').value
                // active:document.getElementById('active').checked

            })
            .then(function (response) {
                // handle success
                console.log(response);
                toastr.success(response.data.message);
                document.getElementById('create-form').rest();
            })
            .catch(function (error) {
                // handle error
                console.log(error);
                toastr.error(error.response.data.message);
            })
            .then(function () {
                // always executed
            });
        }
    </script>
@endsection


