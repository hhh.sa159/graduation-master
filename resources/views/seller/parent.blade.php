<!DOCTYPE html>
<html lang="en" dir="rtl">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.rtl.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.13/css/intlTelInput.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-nice-select/1.1.0/css/nice-select.min.css">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/rtl.css')}}">
    <link rel="stylesheet" href="{{asset('assets/plugins/toastr/toastr.min.css')}}">
    @yield('styles')
    <title>@yield('title')</title>
  </head>
  <body></body>
  <div class="dashboard-layout">
    <div class="dashbord-navbar">
      <div class="logo">  <img src="{{asset('assets/images/logo.png')}}" alt="alt"></div>
      <div class="dash-breadcrumb ms-4">
        <h1> الرئيسية
        </h1>
      </div>
      <div class="dash-actions"><a class="lang-btn">
          <div class="code"> ar </div><span>العربية </span></a>
        <div class="search">
          <form action="#" method="method">
            <div class="form-group">
              <input class="form-control" type="text">
              <button>~<i class="fas fa-search"></i></button>
            </div>
          </form>
        </div>
        <div class="noti">
          <button>
            <svg>
              <use href="{{asset('assets/images/icons/icons.svg#notification')}}"></use>
            </svg>
          </button>
        </div>
        <div class="account-area">
          <div class="dropdown">
            <button class="btn p-0 dropdown-toggle" id="userStatus" type="button" data-bs-toggle="dropdown" aria-expanded="false"><img src="{{asset('assets/images/photo-top.png')}}" alt=""></button>
            <ul class="dropdown-menu" aria-labelledby="userStatus">
              <li><a class="dropdown-item" href="#">
                  <h5>Matgar One</h5><span>46313</span></a></li>
              <li><a class="dropdown-item" href="#">
                  <h5>{{ auth()->user()->name }}</h5><span>{{ auth()->user()->email }}</span></a></li>
              <li><a class="dropdown-item" href="{{route('logout.seller')}}">
                  <h5>تسجيل الخروج</h5></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div id="main">
      <div class="dash-sidebar">
        <div class="top-sidebar">
          <div class="logo">  <img src="{{asset('assets/images/logo.png')}}" alt="alt"></div>
          <div class="close-menu"> <i class="fas fa-times">    </i></div>
        </div>
        <div class="list-links">
                <div class="list-links-group">
                  <h5></h5>
                  <ul>
                    <li><a class="active" href="#">
                        <div class="icon"> <img src="{{asset('assets/images/menu/home.svg')}}" alt=""></div><span>الرئيسية</span></a></li>
                  </ul>
                </div>
                <div class="list-links-group">
                  <h5>المنتجات</h5>
                  <ul>
                    <li><a href="#">
                        <div class="icon"> <img src="{{asset('assets/images/menu/Product_Package.svg')}}" alt=""></div><span>المنتجات </span></a></li>
                    <li><a href="{{route('categories.index')}}">
                        <div class="icon"> <img src="{{asset('assets/images/menu/plus.svg')}}" alt=""></div><span>التصنيفات </span></a></li>
                  </ul>
                </div>
                <div class="list-links-group">
                  <h5>الطلبات</h5>
                  <ul>
                    <li><a href="../dashboard/order-2.html">
                        <div class="icon"> <img src="{{asset('assets/images/menu/Shopping_Bag.svg')}}" alt=""></div><span>الطلبات </span></a></li>
                    <li><a href="../dashboard/bargains.html">
                        <div class="icon"> <img src="{{asset('assets/images/menu/Tag.svg')}}" alt=""></div><span>الصفقات </span></a></li>
                  </ul>
                </div>
                <div class="list-links-group">
                  <h5>التسويق</h5>
                  <ul>
                    <li><a href="../dashboard/Abandoned-baskets.html">
                        <div class="icon"> <img src="{{asset('assets/images/menu/cart.svg')}}" alt=""></div><span>السلات المتروكة </span></a></li>
                  </ul>
                </div>
                <div class="list-links-group">
                  <h5>العملاء</h5>
                  <ul>
                    <li><a href="../dashboard/clients.html">
                        <div class="icon"> <img src="{{asset('assets/images/menu/users.svg')}}" alt=""></div><span>العملاء </span></a></li>
                    <li><a href="../dashboard/chat.html">
                        <div class="icon"> <img src="{{asset('assets/images/menu/comm-chat.svg')}}" alt=""></div><span>الرسائل </span></a></li>
                  </ul>
                </div>
                <div class="list-links-group">
                  <h5>الإعدادات</h5>
                  <ul>
                    <li><a href="../dashboard/shipping-delivery.html">
                        <div class="icon"> <img src="{{asset('assets/images/menu/fast.svg')}}" alt=""></div><span>الشحن والتوصيل </span></a></li>
                    <li><a href="../dashboard/storage-locations.html">
                        <div class="icon"> <img src="{{asset('assets/images/menu/shed.svg')}}" alt=""></div><span>مواقع التخزين  </span></a></li>
                    <li><a href="../dashboard/payments.html">
                        <div class="icon"> <img src="{{asset('assets/images/menu/wallet.svg')}}" alt=""></div><span>طرق الدفع </span></a></li>
                    <li><a href="../dashboard/stocked.html">
                        <div class="icon"> <img src="{{asset('assets/images/menu/shipping.svg')}}" alt=""></div><span>المخزون  </span></a></li>
                    <li><a href="../dashboard/team.html">
                        <div class="icon"> <img src="{{asset('assets/images/menu/users.svg')}}" alt=""></div><span>إدارة الفريق  </span></a></li>
                  </ul>
                </div>
        </div>
      </div>
      @yield('main-content')
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"> </script>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"> </script>
    <script src="{{asset('assets/js/jquery-pincode-autotab.min.js')}}"> </script>
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"> </script>
    <script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.min.js"> </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.13/js/intlTelInput.min.js"> </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"> </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-nice-select/1.1.0/js/jquery.nice-select.min.js"> </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"> </script>
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"> </script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.min.js"></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDSbSmPAGJ9yjR_hfPXnHBH_5zplpE_5sY&amp;callback=initAutocomplete&amp;libraries=places&amp;v=weekly" defer></script>
    <script src="{{asset('assets/js/bundle.min.js')}}"> </script>
    <script src="{{asset('assets/js/moreFields.js')}}"> </script>
    <script src="{{asset('assets/js/main.js')}}"> </script>
    <script src="{{asset('assets/js/charts.js')}}"> </script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="{{asset('assets/plugins/toastr/toastr.min.js')}}"></script>
    <script>
      var options = {
          series: [
              {
                  name: "أرباح",
                  data: [31, 40, 28, 51, 42, 109, 100],
              },
              {
                  name: "مدفوعات",
                  data: [11, 32, 45, 32, 34, 52, 41],
              },
          ],
          chart: {
              height: 350,
              type: "area",
          },
          dataLabels: {
              enabled: false,
          },
          stroke: {
              curve: "smooth",
          },
          toolbar: {
              show: false,
          },
          legend: {
              position: "top",
          },
          colors: ["#fdb933", "#eb631e"],
          xaxis: {
              categories: [
                  "فبراير",
                  "مارس",
                  "ابريل",
                  "يوليو",
                  "يونيو",
                  "اغسطس",
                  "نوفمبر",
                  "اكتوبر",
                  "سبتمبر",
              ],
          },
          tooltip: {
              x: {
                  format: "dd/MM/yy HH:mm",
              },
          },
      };
      const chart1 = document.querySelector("#chart1")


      if (chart1) {
          var chart = new ApexCharts(chart1, options);

          chart.render();
      }
    </script>
    @yield('scripts')
  </div>
</html>
