@extends('dashboard.parent')

@section('title','Create Product')

@section('main-content')

<div class="main-content">
    <div class="sidbar-icon"> <i class="fas fa-bars"> </i></div>
    <ul class="nav nav-tabs d-none">
      <li class="nav-item" role="presentation">
        <button class="nav-link active" id="step-1-details-tab" data-bs-toggle="tab" data-bs-target="#step-1-details" type="button" role="tab" aria-controls="step-1-details" aria-selected="true">Home</button>
      </li>
      <li class="nav-item" role="presentation">
        <button class="nav-link" id="step-2-details-tab" data-bs-toggle="tab" data-bs-target="#step-2-details" type="button" role="tab" aria-controls="step-2-details" aria-selected="false">Profile</button>
      </li>
      <li class="nav-item" role="presentation">
        <button class="nav-link" id="step-3-details-tab" data-bs-toggle="tab" data-bs-target="#step-3-details" type="button" role="tab" aria-controls="step-3-details" aria-selected="false">Messages</button>
      </li>
    </ul>
    <div class="add-product">
      <div class="add-product-head">
        <h1>إضافة منتج جديد</h1>
        <div class="custom-progress-parent step-1">
          <div class="custom-progress-bar">
            <div class="custom-progress-child"> </div>
          </div>
          <div class="custom-progress-text">
            <h5>وصف المنتج </h5>
            <h5>مواصفات المنتج</h5>
            <h5>سعر المنتج </h5>
          </div>
        </div>
      </div>
      <form>
        @csrf
        <div class="tab-content">
          <div class="tab-pane fade show active" id="step-1-details" role="tabpanel" aria-labelledby="step-1-details-tab">
            <div class="add-product-body">
              <div class="add-product-title">
                <h2>صور المنتج </h2>
                <p>يمكنك إضافة صور بحد أقصى 4 صور للمنتج <span class="textt-red mr-2">مقاس 1080*1080</span></p>
              </div>
              <div class="row row-cols-1 mb-5 row-cols-sm-2 row-cols-md-4">
                <div class="col">
                  <div class="image-upload">
                    <div class="check-image">
                      <label for="radio-24">اجعلها صورة رئيسية</label>
                      <div class="form-check">
                        <input class="form-check-input" id="radio-24" type="radio" name="image-check"/>
                        <label class="form-check-label" for="radio-24"></label>
                      </div>
                    </div>
                    <div class="input-image">
                      <input type="file"/><img class="preview-image" src="" alt=""/>
                      <div class="icon">
                        <svg>
                          <use href="{{asset('assets/images/icons/icons.svg#gallery')}}"></use>
                        </svg>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col">
                  <div class="image-upload">
                    <div class="check-image">
                      <label for="radio-960">اجعلها صورة رئيسية</label>
                      <div class="form-check">
                        <input class="form-check-input" id="radio-960" type="radio" name="image-check"/>
                        <label class="form-check-label" for="radio-960"></label>
                      </div>
                    </div>
                    <div class="input-image">
                      <input type="file"/><img class="preview-image" src="" alt=""/>
                      <div class="icon">
                        <svg>
                          <use href="{{asset('assets/images/icons/icons.svg#gallery')}}"></use>
                        </svg>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col">
                  <div class="image-upload">
                    <div class="check-image">
                      <label for="radio-798">اجعلها صورة رئيسية</label>
                      <div class="form-check">
                        <input class="form-check-input" id="radio-798" type="radio" name="image-check"/>
                        <label class="form-check-label" for="radio-798"></label>
                      </div>
                    </div>
                    <div class="input-image">
                      <input type="file"/><img class="preview-image" src="" alt=""/>
                      <div class="icon">
                        <svg>
                          <use href="{{asset('assets/images/icons/icons.svg#gallery')}}"></use>
                        </svg>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col">
                  <div class="image-upload">
                    <div class="check-image">
                      <label for="radio-848">اجعلها صورة رئيسية</label>
                      <div class="form-check">
                        <input class="form-check-input" id="radio-848" type="radio" name="image-check"/>
                        <label class="form-check-label" for="radio-848"></label>
                      </div>
                    </div>
                    <div class="input-image">
                      <input type="file"/><img class="preview-image" src="" alt=""/>
                      <div class="icon">
                        <svg>
                          <use href="{{asset('assets/images/icons/icons.svg#gallery')}}"></use>
                        </svg>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="add-product-title">
                <h2>وصف المنتج</h2>
              </div>
              <div class="row row-cols-1 row-cols-md-2">
                <div class="col">
                  <div class="form-group">
                    <label for="input-996">القسم الرئيسي</label>
                    <select class="niceselect" type="text" id="part">
                      <option value="إختر">اختر </option>
                      <option value="إختر">القسم الأول  </option>
                      <option value="إختر">القسم الثاني  </option>
                      <option value="إختر">القسم الثالث  </option>
                    </select>
                  </div><a class="text-danger text-decoration-none mb-4 d-block" href="" style="margin-top:-15px">تصنيف غير موجود (إضافة تصنيف جديد)</a>
                </div>
                <div class="col">
                  <div class="form-group">
                    <label for="input-535">القسم الفرعي</label>
                    <select class="niceselect" type="text" id="select-535">
                      <option value="إختر">اختر   </option>
                      <option value="إختر">القسم الأول  </option>
                      <option value="إختر">القسم الثاني  </option>
                      <option value="إختر">القسم الثالث  </option>
                    </select>
                  </div>
                </div>
                <div class="col">
                  <div class="input-number">
                    <div class="form-group">
                      <label for="input-621">الكمية المتاحة</label>
                      <div class="input-number-control"><a class="input-increment" href="#"> +</a>
                        <input type="text" id="qulity" placeholder="0" value="0"/><a class="input-decrement" href="#"> -</a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col">
                  <div class="form-group">
                    <label for="input-381">SKU</label>
                    <input class="form-control" type="text" id="name" placeholder="إكتب الإسم"/>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="input-629">ما هي الحد الأدنى من الكمية للإشعار</label>
                <input class="form-control" type="text" id="input-629"/>
              </div>
              <div class="custom-js-tabs">
                <ul class="nav nav-tabs form-lang-tabs" role="tablist" id="tabs-292">
                  <li class="nav-item" role="presentation">
                    <button class="nav-link active" data-tab="arabic-tab" type="button" role="tab">العربية</button>
                  </li>
                  <li class="nav-item" role="presentation">
                    <button class="nav-link" data-tab="english-tab" type="button" role="tab">English</button>
                  </li>
                </ul>
                <div class="tab-content" id="tabs-292Content">
                  <div class="tab-pane fade show active arabic-tab" role="tabpanel">
                    <div class="form-group">
                      <label for="input-325">الإسم</label>
                      <input class="form-control" type="text" id="input-325" placeholder="أكتب الإسم"/>
                    </div>
                  </div>
                  <div class="tab-pane fade english-tab" role="tabpanel">
                    <div class="form-group">
                      <label for="input-543">الإسم</label>
                      <input class="form-control" type="text" id="input-543" placeholder="أكتب الإسم"/>
                    </div>
                  </div>
                </div>
              </div>
              <div class="custom-js-tabs">
                <ul class="nav nav-tabs form-lang-tabs" role="tablist" id="tabs-755">
                  <li class="nav-item" role="presentation">
                    <button class="nav-link active" data-tab="arabic-tab" type="button" role="tab">العربية</button>
                  </li>
                  <li class="nav-item" role="presentation">
                    <button class="nav-link" data-tab="english-tab" type="button" role="tab">English</button>
                  </li>
                </ul>
                <div class="tab-content" id="tabs-755Content">
                  <div class="tab-pane fade show active arabic-tab" role="tabpanel">
                    <div class="form-group">
                      <label for="input-7">وصف المنتج</label>
                      <textarea class="form-control" rows="5"></textarea>
                    </div>
                  </div>
                  <div class="tab-pane fade english-tab" role="tabpanel">
                    <div class="form-group">
                      <label for="input-217">وصف المنتج</label>
                      <textarea class="form-control" rows="5"></textarea>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="add-product-footer">
              <button class="btnn-orange btn btnn ml-2 go-to-step-2 hvr-shadow" type="button"><span>التالي </span>
              </button>
              <button class="btnn-red btn btnn hvr-shadow" type="button"><span>إلغاء </span>
              </button>
            </div>
          </div>
          <div class="tab-pane fade" id="step-2-details" role="tabpanel" aria-labelledby="step-2-details-tab">
            <div class="add-product-body">
              <div class="add-product-title">
                <h2>مواصفات المنتج</h2>
                <p>هي مواصفات تسهل على المستخدم إتخاذ قرار الشراء مثل الحجم - الوزن - اللون</p>
              </div>
              <div id="repeater">
                <div id="structure" style="display:none">
                  <div class="row row-cols-1 row-cols-md-2 align-items-end">
                    <div class="col">
                      <div class="form-group">
                        <label for="input-703">إختر الصفة</label>
                        <select class="niceselect" type="text" id="select-703">
                          <option value="إختر">إختر  </option>
                          <option value="إختر">الصفة الأولى  </option>
                          <option value="إختر">الصفة الثانية  </option>
                          <option value="إختر">الصفة الثالثة  </option>
                        </select>
                      </div>
                    </div>
                    <div class="col">
                      <div class="custom-js-tabs">
                        <ul class="nav nav-tabs form-lang-tabs" role="tablist" id="tabs-118">
                          <li class="nav-item" role="presentation">
                            <button class="nav-link active" data-tab="arabic-tab" type="button" role="tab">العربية</button>
                          </li>
                          <li class="nav-item" role="presentation">
                            <button class="nav-link" data-tab="english-tab" type="button" role="tab">English</button>
                          </li>
                        </ul>
                        <div class="tab-content" id="tabs-118Content">
                          <div class="tab-pane fade show active arabic-tab" role="tabpanel">
                            <div class="form-group">
                              <label for="input-13">أكتب الوزن</label>
                              <input class="form-control" type="text" id="input-13" placeholder="أكتب الإسم"/>
                            </div>
                          </div>
                          <div class="tab-pane fade english-tab" role="tabpanel">
                            <div class="form-group">
                              <label for="input-613">أكتب الوزن</label>
                              <input class="form-control" type="text" id="input-613" placeholder="أكتب الإسم"/>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div id="containerElement"></div><a class="load-more-sec" id="createElement">
                  <p>أضف المزيد من المواصفات</p></a>
              </div>
            </div>
            <div class="add-product-footer">
              <button class="btnn-orange btn btnn ml-2 go-to-step-3 hvr-shadow" type="button"><span>التالي </span>
              </button>
              <button class="btnn-red btn btnn hvr-shadow" type="button"><span>إلغاء </span>
              </button>
            </div>
          </div>
          <div class="tab-pane fade" id="step-3-details" role="tabpanel" aria-labelledby="step-3-details-tab">
            <div class="add-product-body">
              <div class="add-product-title">
                <h2>سعر المنتج</h2>
                <p>ضع سعراً مناسباً لسلعتك</p>
              </div>
              <div class="row row-cols-1 row-cols-md-2 align-items-start">
                <div class="col">
                  <div class="form-group">
                    <label for="input-78">سعر التكلفة</label>
                    <input class="form-control" type="text" id="price"/>
                    <div class="value-text">دينار</div>
                  </div>
                  <div class="d-flex justify-content-between" style="margin-top:-10px">
                    <div class="form-group">
                      <label for="flexCheck">هل السعر خاضع للضريبة </label>
                    </div>
                    <div class="form-check">
                      <input class="form-check-input size-sm" id="flexCheck" type="checkbox" value="">
                      <label class="form-check-label" for="flexCheck"></label>
                    </div>
                  </div>
                </div>
                <div class="col">
                  <div class="form-group">
                    <label for="input-989">نسبة الخصم</label>
                    <input class="form-control" type="text" id="input-989"/>
                    <div class="value-text">%</div>
                  </div>
                </div>
                <div class="col"> </div>
              </div>
              <div class="form-group">
                <label for="input-340">السعر بعد الخصم</label>
                <input class="form-control" type="text" id="input-340"/>
                <div class="value-text">دينار</div>
              </div>
              <div class="form-group">
                <label for="input-218">الحد الأدنى للطلب</label>
                <input class="form-control" type="text" id="input-218"/>
                <div class="value-text">دينار</div>
              </div>
              <div class="d-flex justify-content-between" style="margin-top:-10px">
                <div class="form-group">
                  <label for="switch5"> السعر  الذكي كل ما زادت الكمية قل السعر </label>
                </div>
                <div class="form-check form-switch form-switch-md">
                  <input class="form-check-input" type="checkbox" id="switch5" role="switch">
                  <label class="form-check-label" for="switch5"></label>
                </div>
              </div>
              <div class="add-more-sec">
                <div class="row input_fields_wrap">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for=""> الكمية</label>
                      <div class="d-flex justify-content-between flex-wrap">
                        <div class="d-flex align-items-center mb-2">
                          <label style="min-width:70px"> من</label>
                          <input class="form-control" type="text">
                        </div>
                        <div class="d-flex align-items-center mb-2">
                          <label style="min-width:70px"> إلى</label>
                          <input class="form-control" type="text">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="input-547">السعر</label>
                      <input class="form-control" type="text" id="input-547"/>
                      <div class="value-text">دينار</div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12">
                    <button class="btnn-orange btn btnn add_field_button hvr-shadow" type="button"><span>إضافة بند آخر</span>
                    </button>
                  </div>
                </div>
              </div>
              <div class="d-flex justify-content-between mt-5">
                <div class="add-product-title">
                  <h2> عرض المنتج</h2>
                  <p>  هل تريد عرض المنتج في الزونات </p>
                </div>
                <div class="form-check form-switch form-switch-md">
                  <input class="form-check-input" type="checkbox" id="switch6" role="switch">
                  <label class="form-check-label" for="switch6"></label>
                </div>
              </div>
              <div class="form-group">
                <label for="input-883"> </label>
                <select class="niceselect" type="text" id="select-883">
                  <option value="إختر">إختر  </option>
                  <option value="إختر">الزون الأول  </option>
                  <option value="إختر">الزون الثاني  </option>
                  <option value="إختر">الزون الثالث  </option>
                </select>
              </div>
              <div class="form-group">
                <label for="zoneOptions">  إختر زون من الزونات المتاحة</label>
                <div class="cus-multi-select" id="zoneOptions" data-options="[{&quot;label&quot;: &quot;New York&quot;,&quot;value&quot;: &quot;NY&quot;},{&quot;label&quot;: &quot;Washington&quot;,&quot;value&quot;: &quot;WA&quot;},{&quot;label&quot;: &quot;California&quot;,&quot;value&quot;: &quot;CA&quot;},{&quot;label&quot;: &quot;New Jersey&quot;,&quot;value&quot;: &quot;NJ&quot;},{&quot;label&quot;: &quot;North Carolina&quot;,&quot;value&quot;: &quot;NC&quot;}]"> </div>
              </div>
            </div>
            <div class="add-product-footer">
              <button type="button" onclick="store()" class="btnn-orange btn btnn ml-2 hvr-shadow">إضافة</button>
              <button class="btnn-red btn btnn hvr-shadow" type="button"><span>إلغاء </span>
              </button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>


@endsection

@section('scripts')
    <script>

        function store(){
            axios.post('/cms/client/products',{
                name:document.getElementById('name').value,
                part:document.getElementById('part').value,
                active:document.getElementById('active').checked,
                qulity:document.getElementById('qulity').value,
                price:document.getElementById('price').value,

            })
            .then(function (response) {
                // handle success
                console.log(response);
                toastr.success(response.data.message);
                window.location.href = "/cms/client/products";
            })
            .catch(function (error) {
                // handle error
                console.log(error);
                toastr.error(error.response.data.message);
            })
            .then(function () {
                // always executed
            });
        }
    </script>
@endsection

