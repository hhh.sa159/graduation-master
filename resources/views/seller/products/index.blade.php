@extends('dashboard.parent')

@section('title','Total Products')

@section('main-content')

<div class="main-content">
    <div class="sidbar-icon"> <i class="fas fa-bars"> </i></div>
    <div class="dash-page-head">
      <h2>إجمالي المنتجات <span>( 555 )</span></h2><a class="main-link btn hvr-float-shadow" href="{{route('products.create')}}">إضافى منتج جديد</a>
    </div>
    <div class="page-filters-2">
      <div class="inputs-btn">
        <div class="form-group">
          <select class="niceselect" type="text" id="select-444">
            <option value="الأحدث">الأحدث  </option>
            <option value="إختر">إختر  </option>
            <option value="إختر">إختر  </option>
            <option value="الأحدث">الأحدث  </option>
          </select>
        </div>
        <div class="form-group">
          <select class="niceselect" type="text" id="select-658">
            <option value="حسب القسم">حسب القسم  </option>
            <option value="إختر">إختر  </option>
            <option value="إختر">إختر  </option>
            <option value="حسب القسم">حسب القسم  </option>
          </select>
        </div>
        <button class="btnn-orange btn btnn ml-2 go-to-step-3 btn-fill hvr-shadow" type="button"><span>التالي </span>
        </button>
      </div>
      <div class="table-search-box">
        <input class="form-control" type="text">
        <div class="icon">
          <svg>
            <use href="../../assets/images/icons/icons.svg#search"></use>
          </svg>
        </div>
      </div>
    </div>
    <div class="row row-cols-lg-4 row-cols-2 mt-5">
      <div class="col">
        <div class="status-info-box"><span class="orange">منتج بالفعل</span>
          <h4>450 منتج</h4>
        </div>
      </div>
      <div class="col">
        <div class="status-info-box"><span class="green">بإنتظار المراجعة</span>
          <h4>450 منتج</h4>
        </div>
      </div>
      <div class="col">
        <div class="status-info-box"><span class="red">أوشك على النفاذ</span>
          <h4>450 منتج</h4>
        </div>
      </div>
      <div class="col">
        <div class="status-info-box"><span class="red">مرفوض</span>
          <h4>450 منتج</h4>
        </div>
      </div>
    </div>
    <div class="table-filters">
      <div class="form-group mx-1 mb-0">
        <select class="niceselect" type="text" id="select-404">
          <option value="القسم ">القسم   </option>
          <option value="إختر">إختر  </option>
          <option value="إختر">إختر  </option>
          <option value="إختر">إختر  </option>
          <option value="إختر">إختر  </option>
        </select>
      </div>
      <div class="btn-filter">
        <button class="btn hvr-shadow">فلترة</button>
      </div>
      <div class="table-search-box">
        <input class="form-control" type="text"/>
        <div class="icon">
          <svg>
            <use href="../../assets/images/icons/icons.svg#search"></use>
          </svg>
        </div>
      </div>
    </div>
    <div class="table-container inner-padding">
      <div class="table-inner-head">
        <h3>جميع المنتجات</h3>
        <div class="tabl-head-buttons">
          <button class="btnn-undefined btn btnn btnn-orange hvr-shadow" type="button"><span>تصدير</span>
            <svg>
              <use href="../../assets/images/icons/icons.svg#download"></use>
            </svg>
          </button>
          <button class="btnn-undefined btn btnn btnn-gray hvr-shadow" type="button"><span>تحديث</span>
            <svg>
              <use href="../../assets/images/icons/icons.svg#refresh"></use>
            </svg>
          </button>
        </div>
      </div>
      <div class="dash-table table-responsive">
        <table class="table responsive" id="">
          <thead>
            <th> المنتج</th>
            <th>الإسم</th>
            <th>القسم</th>
            <th>الحالة</th>
            <th>الكمية</th>
            <th>سعر المنتج</th>
            <th>الإجراءات</th>
          </thead>
          <tbody>
              @foreach ($products as $product )
              <tr>
                <td><div class="table-img"><img src="../../assets/images/product-thumb.png" /></div></td>
                <td>{{$product->name}}</td>
                <td>{{$product->part}}</td>
                <td><span class="badge @if($product->active) bg-success @else bg-danger @endif">{{$product->status}}</span></td>
                <td>{{$product->qulity}}</td>
                <td>{{$product->price}}</td>
                <td>
                    <div class="btn-group">
                        <a href="{{route('products.edit',$product->id)}}" class="btn btn-info">
                            <i class="far fa-edit"></i>
                        </a>
                        <a href="#" class="btn btn-danger"
                        onclick="confirmDestroy({{$product->id}},this)">
                            <i class="fas fa-trash"></i>
                        </a>
                      </div>
                </td>
              </tr>
              @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>

@endsection

@section('scripts')

    <script>
        function confirmDestroy(id,reference){
            // alert("CONFIRM DESTROY FUNCTION");
            // console.log("CONFIRM DESTROY");
            // console.log(id);
            Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
            if (result.isConfirmed) {
                destroy(id,reference);
            }
            })
        }
        function destroy(id,reference){
            axios.delete('/cms/client/products/'+id)
            .then(function (response) {
                // handle success
                console.log(response);
                reference.closest('tr').remove();
                showMessage(response.data);
            })
            .catch(function (error) {
                // handle error
                console.log(error);
                showMessage(error.response.data);
            })
            .then(function () {
                // always executed
            });
        }
        function showMessage(data){
            Swal.fire({
                icon: data.icon,
                title: data.title,
                text:data.text,
                showConfirmButton: false,
                timer: 1500
            })
        }
    </script>

@endsection

