@extends('seller.parent')

@section('title','Home')

@section('main-content')

<div class="main-content">
    <div class="sidbar-icon"> <i class="fas fa-bars"> </i></div>
    <div class="home-progress">
      <h2 class="home-progress-title">أكمل إعدادات متجرك</h2>
      <div class="home-progress-bar-parent">
        <div class="home-progress-bar-inner" style="width:57%"></div>
        <h4><span>مكتمل</span><span><div class="counter">57</div>%</span></h4>
      </div>
    </div>
    <div class="row row-cols-lg-4 row-cols-2">
      <div class="col">
        <div class="status-info-box"><span>الارباح</span>
          <h4><div class="counter">3504</div> شيكل</h4>
          <h5>ﻫﺬا اﻟﺎﺳﺒﻮع</h5>
          <div class="status-info-box-icon chart-up">
            <svg>
              <use href="../../assets/images/icons/icons.svg#chart-up"></use>
            </svg><span>+<div class="counter">20</div>%</span>
          </div>
        </div>
      </div>
      <div class="col">
        <div class="status-info-box"><span>الطلبات</span>
          <h4><div class="counter">350</div></h4>
          <h5>ﻫﺬا اﻟﺎﺳﺒﻮع</h5>
          <div class="status-info-box-icon chart-up">
            <svg>
              <use href="../../assets/images/icons/icons.svg#chart-up"></use>
            </svg><span>+<div class="counter">20</div>%</span>
          </div>
        </div>
      </div>
      <div class="col">
        <div class="status-info-box"><span>ﻋﺮوض اﻟﺼﻔﻘﺎت</span>
          <h4><div class="counter">3540</div> </h4>
          <h5>ﻫﺬا اﻟﺎﺳﺒﻮع</h5>
          <div class="status-info-box-icon chart-down">
            <svg>
              <use href="../../assets/images/icons/icons.svg#chart-down"></use>
            </svg><span>+<div class="counter">20</div>%</span>
          </div>
        </div>
      </div>
      <div class="col">
        <div class="status-info-box"><span>ﻣﺸﺎﻫﺪات ﻗﺼﺼﻰ</span>
          <h4><div class="counter">3505</div> </h4>
          <h5>ﻫﺬا اﻟﺎﺳﺒﻮع</h5>
          <div class="status-info-box-icon chart-up">
            <svg>
              <use href="../../assets/images/icons/icons.svg#chart-up"></use>
            </svg><span>+<div class="counter">20</div>%</span>
          </div>
        </div>
      </div>
    </div>
    <div class="row match-height">
      <div class="col-lg-4">
        <div class="custom-cart">
          <div class="custom-cart-header">
            <h2> أﻛﺜﺮ اﻟﻌﻤﻠﺎء ﺷﺮاءً</h2>
          </div>
          <div class="custom-cart-body">
            <ul class="clitens-link">
              <li>
                <div class="img"> <img src="../../assets/images/photo-top.png" alt=""></div>
                <div class="name">
                  <h4> Mohamed ahmed</h4><span>  ﻋﻤﻴﻞ ﻣﻨﺬ 2021/4/21</span>
                </div>
                <div class="paid">SR 64.00</div>
              </li>
              <li>
                <div class="img"> <img src="../../assets/images/photo-top.png" alt=""></div>
                <div class="name">
                  <h4> Mohamed ahmed</h4><span>  ﻋﻤﻴﻞ ﻣﻨﺬ 2021/4/21</span>
                </div>
                <div class="paid">SR 64.00</div>
              </li>
              <li>
                <div class="img"> <img src="../../assets/images/photo-top.png" alt=""></div>
                <div class="name">
                  <h4> Mohamed ahmed</h4><span>  ﻋﻤﻴﻞ ﻣﻨﺬ 2021/4/21</span>
                </div>
                <div class="paid">SR 64.00</div>
              </li>
              <li>
                <div class="img"> <img src="../../assets/images/photo-top.png" alt=""></div>
                <div class="name">
                  <h4> Mohamed ahmed</h4><span>  ﻋﻤﻴﻞ ﻣﻨﺬ 2021/4/21</span>
                </div>
                <div class="paid">SR 64.00</div>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-lg-8">
        <div class="custom-cart">
          <div class="custom-cart-header">
            <h2>ارﺑﺎح ﻣﺘﺠﺮك</h2>
          </div>
          <div class="custom-cart-body">
            <div id="chart1"> </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row match-height mt-4">
      <div class="col-lg-12">
        <div class="custom-cart">
          <div class="custom-cart-header">
            <h2> اﻟﻄﻠﺒﺎت اﻟﺠﺪﻳﺪة</h2>
            <button class="btnn-orange btn btnn hvr-shadow" type="button"><span>رؤية الكل</span>
            </button>
          </div>
          <div class="custom-cart-body">
            <div class="table-container">
              <div class="dash-table table-responsive">
                <table class="table responsive" id="">
                  <thead>
                    <th>رقم الطلب</th>
                    <th>العميل</th>
                    <th>تاريخ الطلب</th>
                    <th>نوع الطلب</th>
                    <th>الشحن</th>
                    <th>الدفع</th>
                    <th>حالة الطلب</th>
                    <th>المجموع</th>
                    <th>الإجراءات</th>
                  </thead>
                  <tbody>
                    <tr>
                      <td>#6461313</td>
                      <td><div class="table-two-texts"><h5>Mohamed ahmed</h5><h6>عميل منذ 21/14/2021</h6></div></td>
                      <td><div class="table-two-texts"><h5>12 سبمتمر 2021</h5><h6>4:30 مساءً</h6></div></td>
                      <td><h5>صفقة</h5></td>
                      <td><div class="table-two-texts"><h5>سبلايز</h5><h6 class="express">Express</h6></div></td>
                      <td><h5>كاش</h5></td>
                      <td><div class="table-two-texts"><h5 class="textt-orange">بإنتظار التأكيد</h5><h6>4:30 مساءً</h6></div></td>
                      <td><h5>64 شيكل</h5></td>
                      <td>
                        <div class="table-actions"><a class="table-action-btn" href="#">
                            <svg>
                              <use href="../../assets/images/icons/icons.svg#eye"></use>
                            </svg></a><a class="table-action-btn" href="#">
                            <svg>
                              <use href="../../assets/images/icons/icons.svg#download"></use>
                            </svg></a>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>#6461313</td>
                      <td><div class="table-two-texts"><h5>Mohamed ahmed</h5><h6>عميل منذ 21/14/2021</h6></div></td>
                      <td><div class="table-two-texts"><h5>12 سبمتمر 2021</h5><h6>4:30 مساءً</h6></div></td>
                      <td><h5>صفقة</h5></td>
                      <td><div class="table-two-texts"><h5>سبلايز</h5><h6 class="express">Express</h6></div></td>
                      <td><h5>كاش</h5></td>
                      <td><div class="table-two-texts"><h5 class="textt-orange">بإنتظار التأكيد</h5><h6>4:30 مساءً</h6></div></td>
                      <td><h5>64 شيكل</h5></td>
                      <td>
                        <div class="table-actions"><a class="table-action-btn" href="#">
                            <svg>
                              <use href="../../assets/images/icons/icons.svg#eye"></use>
                            </svg></a><a class="table-action-btn" href="#">
                            <svg>
                              <use href="../../assets/images/icons/icons.svg#download"></use>
                            </svg></a>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>#6461313</td>
                      <td><div class="table-two-texts"><h5>Mohamed ahmed</h5><h6>عميل منذ 21/14/2021</h6></div></td>
                      <td><div class="table-two-texts"><h5>12 سبمتمر 2021</h5><h6>4:30 مساءً</h6></div></td>
                      <td><h5>صفقة</h5></td>
                      <td><div class="table-two-texts"><h5>سبلايز</h5><h6 class="express">Express</h6></div></td>
                      <td><h5>كاش</h5></td>
                      <td><div class="table-two-texts"><h5 class="textt-orange">بإنتظار التأكيد</h5><h6>4:30 مساءً</h6></div></td>
                      <td><h5>64 شيكل</h5></td>
                      <td>
                        <div class="table-actions"><a class="table-action-btn" href="#">
                            <svg>
                              <use href="../../assets/images/icons/icons.svg#eye"></use>
                            </svg></a><a class="table-action-btn" href="#">
                            <svg>
                              <use href="../../assets/images/icons/icons.svg#download"></use>
                            </svg></a>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>#6461313</td>
                      <td><div class="table-two-texts"><h5>Mohamed ahmed</h5><h6>عميل منذ 21/14/2021</h6></div></td>
                      <td><div class="table-two-texts"><h5>12 سبمتمر 2021</h5><h6>4:30 مساءً</h6></div></td>
                      <td><h5>صفقة</h5></td>
                      <td><div class="table-two-texts"><h5>سبلايز</h5><h6 class="express">Express</h6></div></td>
                      <td><h5>كاش</h5></td>
                      <td><div class="table-two-texts"><h5 class="textt-orange">بإنتظار التأكيد</h5><h6>4:30 مساءً</h6></div></td>
                      <td><h5>64 شيكل</h5></td>
                      <td>
                        <div class="table-actions"><a class="table-action-btn" href="#">
                            <svg>
                              <use href="../../assets/images/icons/icons.svg#eye"></use>
                            </svg></a><a class="table-action-btn" href="#">
                            <svg>
                              <use href="../../assets/images/icons/icons.svg#download"></use>
                            </svg></a>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row match-height mt-4">
      <div class="col-lg-6">
        <div class="custom-cart">
          <div class="custom-cart-header">
            <h2>أكثر المنتجات مبيعاً</h2>
            <button class="btnn-orange btn btnn hvr-shadow" type="button"><span>رؤية الكل</span>
            </button>
          </div>
          <div class="custom-cart-body">
            <div class="table-container">
              <div class="dash-table table-responsive">
                <table class="table responsive" id="">
                  <thead>
                    <th>المنتج</th>
                    <th>الإسم</th>
                    <th>القسم</th>
                    <th>الحالة</th>
                    <th>الكمية</th>
                    <th>سعر المنتج</th>
                    <th>الإجراءات</th>
                  </thead>
                  <tbody>
                    <tr>
                      <td><div class="table-img"><img src="../../assets/images/product-thumb.png" /></div></td>
                      <td><div class="table-two-texts"><h5>خضراوات مشكل</h5><h6>#54545454</h6></div></td>
                      <td><div class="table-two-texts"><h5>بقالة</h5><h6> خضراوات </h6></div></td>
                      <td><h5 class="textt-orange">متاح</h5></td>
                      <td><h5>45 </h5></td>
                      <td><h5>45 شيكل</h5></td>
                      <td>
                        <div class="table-actions"><a class="table-action-btn" href="#">
                            <svg>
                              <use href="../../assets/images/icons/icons.svg#eye"></use>
                            </svg></a><a class="table-action-btn" href="#">
                            <svg>
                              <use href="../../assets/images/icons/icons.svg#download"></use>
                            </svg></a>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td><div class="table-img"><img src="../../assets/images/product-thumb.png" /></div></td>
                      <td><div class="table-two-texts"><h5>خضراوات مشكل</h5><h6>#54545454</h6></div></td>
                      <td><div class="table-two-texts"><h5>بقالة</h5><h6> خضراوات </h6></div></td>
                      <td><h5 class="textt-orange">متاح</h5></td>
                      <td><h5>45 </h5></td>
                      <td><h5>45 شيكل</h5></td>
                      <td>
                        <div class="table-actions"><a class="table-action-btn" href="#">
                            <svg>
                              <use href="../../assets/images/icons/icons.svg#eye"></use>
                            </svg></a><a class="table-action-btn" href="#">
                            <svg>
                              <use href="../../assets/images/icons/icons.svg#download"></use>
                            </svg></a>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td><div class="table-img"><img src="../../assets/images/product-thumb.png" /></div></td>
                      <td><div class="table-two-texts"><h5>خضراوات مشكل</h5><h6>#54545454</h6></div></td>
                      <td><div class="table-two-texts"><h5>بقالة</h5><h6> خضراوات </h6></div></td>
                      <td><h5 class="textt-orange">متاح</h5></td>
                      <td><h5>45 </h5></td>
                      <td><h5>45 شيكل</h5></td>
                      <td>
                        <div class="table-actions"><a class="table-action-btn" href="#">
                            <svg>
                              <use href="../../assets/images/icons/icons.svg#eye"></use>
                            </svg></a><a class="table-action-btn" href="#">
                            <svg>
                              <use href="../../assets/images/icons/icons.svg#download"></use>
                            </svg></a>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td><div class="table-img"><img src="../../assets/images/product-thumb.png" /></div></td>
                      <td><div class="table-two-texts"><h5>خضراوات مشكل</h5><h6>#54545454</h6></div></td>
                      <td><div class="table-two-texts"><h5>بقالة</h5><h6> خضراوات </h6></div></td>
                      <td><h5 class="textt-orange">متاح</h5></td>
                      <td><h5>45 </h5></td>
                      <td><h5>45 شيكل</h5></td>
                      <td>
                        <div class="table-actions"><a class="table-action-btn" href="#">
                            <svg>
                              <use href="../../assets/images/icons/icons.svg#eye"></use>
                            </svg></a><a class="table-action-btn" href="#">
                            <svg>
                              <use href="../../assets/images/icons/icons.svg#download"></use>
                            </svg></a>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="custom-cart">
          <div class="custom-cart-header">
            <h2>اوﺷﻚ ﻋﻠﻰ اﻟﻨﻔﺎذ</h2>
            <button class="btnn-orange btn btnn hvr-shadow" type="button"><span>رؤية الكل</span>
            </button>
          </div>
          <div class="custom-cart-body">
            <div class="table-container">
              <div class="dash-table table-responsive">
                <table class="table responsive" id="">
                  <thead>
                    <th>المنتج</th>
                    <th>الإسم</th>
                    <th>القسم</th>
                    <th>السعر</th>
                    <th>الكمية</th>
                  </thead>
                  <tbody>
                    <tr>
                      <td><div class="table-img"><img src="../../assets/images/product-thumb.png" /></div></td>
                      <td><div class="table-two-texts"><h5>خضراوات مشكل</h5><h6>#54545454</h6></div></td>
                      <td><div class="table-two-texts"><h5>بقالة</h5><h6> خضراوات </h6></div></td>
                      <td><h5>45 شيكل</h5></td>
                      <td>
                        <div class="table-actions"><a class="table-action-btn" href="#">
                            <svg>
                              <use href="../../assets/images/icons/icons.svg#eye"></use>
                            </svg></a><a class="table-action-btn" href="#">
                            <svg>
                              <use href="../../assets/images/icons/icons.svg#download"></use>
                            </svg></a>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td><div class="table-img"><img src="../../assets/images/product-thumb.png" /></div></td>
                      <td><div class="table-two-texts"><h5>خضراوات مشكل</h5><h6>#54545454</h6></div></td>
                      <td><div class="table-two-texts"><h5>بقالة</h5><h6> خضراوات </h6></div></td>
                      <td><h5>45 شيكل</h5></td>
                      <td>
                        <div class="table-actions"><a class="table-action-btn" href="#">
                            <svg>
                              <use href="../../assets/images/icons/icons.svg#eye"></use>
                            </svg></a><a class="table-action-btn" href="#">
                            <svg>
                              <use href="../../assets/images/icons/icons.svg#download"></use>
                            </svg></a>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td><div class="table-img"><img src="../../assets/images/product-thumb.png" /></div></td>
                      <td><div class="table-two-texts"><h5>خضراوات مشكل</h5><h6>#54545454</h6></div></td>
                      <td><div class="table-two-texts"><h5>بقالة</h5><h6> خضراوات </h6></div></td>
                      <td><h5>45 شيكل</h5></td>
                      <td>
                        <div class="table-actions"><a class="table-action-btn" href="#">
                            <svg>
                              <use href="../../assets/images/icons/icons.svg#eye"></use>
                            </svg></a><a class="table-action-btn" href="#">
                            <svg>
                              <use href="../../assets/images/icons/icons.svg#download"></use>
                            </svg></a>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td><div class="table-img"><img src="../../assets/images/product-thumb.png" /></div></td>
                      <td><div class="table-two-texts"><h5>خضراوات مشكل</h5><h6>#54545454</h6></div></td>
                      <td><div class="table-two-texts"><h5>بقالة</h5><h6> خضراوات </h6></div></td>
                      <td><h5>45 شيكل</h5></td>
                      <td>
                        <div class="table-actions"><a class="table-action-btn" href="#">
                            <svg>
                              <use href="../../assets/images/icons/icons.svg#eye"></use>
                            </svg></a><a class="table-action-btn" href="#">
                            <svg>
                              <use href="../../assets/images/icons/icons.svg#download"></use>
                            </svg></a>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
