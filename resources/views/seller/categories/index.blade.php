@extends('seller.parent')

@section('title','Category')

@section('main-content')

<div class="main-content">
    <div class="sidbar-icon"> <i class="fas fa-bars"> </i></div>
    <div class="dash-page-head">
      <h2>التصنيفات <span>( 555 )</span></h2><a class="main-link btn hvr-float-shadow" href="{{route('categories.create')}}">إضافة تصنيف</a>
    </div>
    <div class="table-container inner-padding">
      <div class="table-inner-head">
        <h3>التصنيفات</h3>
        <div class="tabl-head-buttons">
          <button class="btnn-undefined btn btnn btnn-red hvr-shadow" type="button"><span>حذف جميع التصنيفات</span>
          </button>
        </div>
      </div>
      <div class="dash-table">
        <table class="table responsive" id="example">
          <thead>
            <th>الإسم</th>
            <th>الإسم بالانجليزي</th>
            <th>تاريخ الإنشاء</th>
            <th>القسم الرئيسي</th>
            <th>الإجراءات</th>
          </thead>
          <tbody>
              @foreach ($categories as $category)
              <tr>
                <td>{{$category->name_ar}}</td>
                <td>{{$category->name_en}}</td>
                <td>{{$category->created_at->diffForHumans()}}</td>
                <td>{{$category->section}}</td>
                <td>
                    <div class="btn-group">
                        <a href="{{route('categories.edit',$category->id)}}" class="btn btn-info">
                            <i class="far fa-edit"></i>
                        </a>
                        <a href="#" class="btn btn-danger"
                        onclick="confirmDestroy({{$category->id}},this)">
                            <i class="fas fa-trash"></i>
                        </a>
                      </div>
                </td>
              </tr>
              @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>

@endsection

@section('scripts')

    <script>
        function confirmDestroy(id,reference){
            // alert("CONFIRM DESTROY FUNCTION");
            // console.log("CONFIRM DESTROY");
            // console.log(id);
            Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
            if (result.isConfirmed) {
                destroy(id,reference);
            }
            })
        }
        function destroy(id,reference){
            axios.delete('/seller/categories/'+id)
            .then(function (response) {
                // handle success
                console.log(response);
                reference.closest('tr').remove();
                showMessage(response.data);
            })
            .catch(function (error) {
                // handle error
                console.log(error);
                showMessage(error.response.data);
            })
            .then(function () {
                // always executed
            });
        }
        function showMessage(data){
            Swal.fire({
                icon: data.icon,
                title: data.title,
                text:data.text,
                showConfirmButton: false,
                timer: 1500
            })
        }
    </script>

@endsection
