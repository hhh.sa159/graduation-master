@extends('seller.parent')

@section('title', 'Add Category')


@section('main-content')

    <div class="main-content">
        <div class="sidbar-icon"> <i class="fas fa-bars"> </i></div>
        <div class="page-content-inner">
            <div class="add-product">
                <div class="add-product-title">
                    <h2>صور المنتج <a href="">
                            <p>أو إختر من مجموعة الأيقنات لدينا </p>
                        </a></h2>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-4">
                        <div class="image-upload">
                            <div class="input-image">
                                <input type="file" /><img class="preview-image" src="" alt="" />
                                <div class="icon">
                                    <svg>
                                        <use href="../../assets/images/icons/icons.svg#gallery"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="custom-js-tabs">
                    <ul class="nav nav-tabs form-lang-tabs" role="tablist" id="tabs-270">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" data-tab="arabic-tab" type="button" role="tab">العربية</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" data-tab="english-tab" type="button" role="tab">English</button>
                        </li>
                    </ul>
                    <div class="tab-content" id="tabs-270Content">
                        <div class="tab-pane fade show active arabic-tab" role="tabpanel">
                            <div class="form-group">
                                <label for="input-956">الإسم</label>
                                <input class="form-control" type="text" id="name-ar" placeholder="أكتب الإسم" />
                            </div>
                        </div>
                        <div class="tab-pane fade english-tab" role="tabpanel">
                            <div class="form-group">
                                <label for="input-338">الإسم</label>
                                <input class="form-control" type="text" id="name-en" placeholder="أكتب الإسم" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="input-534">القسم الرئيسي</label>
                    <select class="niceselect" type="text" id="section">
                        <option disabled selected>إختر </option>
                        <option value="قسم1">قسم1 </option>
                        <option value="قسم2">قسم2 </option>
                        <option value="قسم3">قسم3 </option>
                    </select>
                </div>
            </div>
            <div class="add-product-footer">
                <button class="btnn-orange btn btnn ml-2 go-to-step-2 hvr-shadow" onclick="store()"><span>إضافة </span>
                </button>
                <button class="btnn-red btn btnn hvr-shadow" type="button"><span>إلغاء </span>
                </button>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        function store() {
          let nameAr = document.getElementById('name-ar').value;
          let nameEn = document.getElementById('name-en').value;
          let section = document.getElementById('section').value;
            axios.post('/seller/categories', {
                    name_ar: nameAr,
                    name_en: nameEn,
                    section: section,
                })
                .then(function(response) {
                    // handle success
                    console.log(response.data);
                    toastr.success(response.data.message);
                    // window.location.href = "/cms/seller/categories";
                })
                .catch(function(error) {
                    // handle error
                    console.log(error);
                    toastr.error(error.response.data.message);
                })

                // axios.post('').then((res)=> {
                //   console.log(res.data);
                // }, (error)=> {
                //   console.log(error);
                // })
        }
    </script>
@endsection
