@extends('seller.parent')

@section('title','Edit Category')


@section('main-content')

<div class="main-content">
    <div class="sidbar-icon"> <i class="fas fa-bars"> </i></div>
    <div class="page-content-inner">
            <div class="form-group">
              <label for="name">الاسم</label>
              <input type="text" class="form-control" id="name" placeholder="أدخل الاسم"
              value="{{$category->name}}">
            </div>
            <div class="form-group">
                <label for="section"> القسم الرئيسي</label>
                <input type="text" class="form-control" id="section" placeholder="أدخل القسم"
                value="{{$category->section}}">
              </div>
              <div class="form-group">
                <label for="active">الحالة</label>
                <input type="text" class="form-control" id="active" placeholder="أدخل الحالة"
                value="{{$category->active}}">
              </div>
          </div>
          <!-- /.card-body -->

          <div class="card-footer">
            <button type="button" class="btn btn-primary" onclick="update({{$category->id}})">Store</button>
          </div>
    </div>
  </div>

@endsection

@section('scripts')
    <script>

        function update(id){
            axios.put('/seller/categories/'+id,{
                name:document.getElementById('name').value,
                section:document.getElementById('section').value,
                active:document.getElementById('active').value
            })
            .then(function (response) {
                // handle success
                console.log(response);
                toastr.success(response.data.message);
                window.location.href = "/seller/categories";
            })
            .catch(function (error) {
                // handle error
                console.log(error);
                toastr.error(error.response.data.message);
            })
            .then(function () {
                // always executed
            });
        }
    </script>
@endsection
