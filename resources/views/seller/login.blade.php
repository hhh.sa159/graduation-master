<!DOCTYPE html>
<html lang="en" dir="rtl">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.rtl.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.13/css/intlTelInput.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-nice-select/1.1.0/css/nice-select.min.css">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/rtl.css')}}">
    <link rel="stylesheet" href="{{asset('assets/plugins/toastr/toastr.min.css')}}">
    <title>Login</title>
  </head>
  <body>
    <div class="auth-layout">
      <div class="container-fluid p-0">
        <div class="row row-cols-1 g-0 row-cols-lg-2">
          <div class="col">
            <div class="auth-side-content">
              <div class="auth-nav">
                <div class="auth-nav-logo"> <img src="{{asset('assets/images/logo.png')}}" alt=""></div>
              </div>
              <div class="auth-header">
                <h2>لنا مكان في  <br> كل دكان</h2>
                <h3>قم بإدارة طلباتك , عروضك , عملائك بسهولة</h3>
              </div>
              <div class="auth-bg"> <img src="{{asset('assets/images/auth-bg.png')}}" alt=""></div>
            </div>
          </div>
          <div class="col">
            <div class="auth-form">
              <div class="icon"> <img src="{{asset('assets/images/icons/waving-hand.svg')}}" alt=""></div>
              <h1>مرحباً . قم بتسجيل الدخول </h1>
              <form>
                  <div class="input-group mb-3">
                    <input type="email" id="email" class="form-control" placeholder="Email">
                    <div class="input-group-append">
                      <div class="input-group-text">
                        <span class="fas fa-envelope"></span>
                      </div>
                    </div>
                  </div>
                  <div class="input-group mb-3">
                    <input type="password" id="password" class="form-control" placeholder="Password">
                    <div class="input-group-append">
                      <div class="input-group-text">
                        <span class="fas fa-lock"></span>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-8">
                      <div class="icheck-primary">
                        <input type="checkbox" id="remember">
                        <label for="remember">
                          Remember Me
                        </label>
                      </div>
                    </div>
                    <!-- /.col -->
                    {{-- <div class="col-4">
                      <button type="button" onclick="login()" class="btn btn-primary btn-block">Sign In</button>
                    </div> --}}
                    <!-- /.col -->
                  </div>
                <div id="error-msg"></div>
                <div id="valid-msg"></div>
                <div class="btn-submit">
                    <button type="button" onclick="login()" class="btn hvr-float-shadow">تسجيل الدخول </button>
                     {{-- <a class="btn hvr-float-shadow" onclick="login()"><span>تسجيل الدخول   </span></a> --}}
                </div>
                <div class="bottom-form-text">
                  <h5>ما اشتركت معنا بعد   <a href="#">إشترك الآن   </a></h5>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"> </script>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"> </script>
    <script src="{{asset('assets/js/jquery-pincode-autotab.min.js')}}"> </script>
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"> </script>
    <script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.min.js"> </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.13/js/intlTelInput.min.js"> </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"> </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-nice-select/1.1.0/js/jquery.nice-select.min.js"> </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"> </script>
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"> </script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.min.js"></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDSbSmPAGJ9yjR_hfPXnHBH_5zplpE_5sY&amp;callback=initAutocomplete&amp;libraries=places&amp;v=weekly" defer></script>
    <script src="{{asset('assets/js/bundle.min.js')}}"> </script>
    <script src="{{asset('assets/js/moreFields.js')}}"> </script>
    <script src="{{asset('assets/js/main.js')}}"> </script>
    <script src="{{asset('assets/js/charts.js')}}"> </script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="{{asset('assets/plugins/toastr/toastr.min.js')}}"></script>

    <script>

        function login(){
            axios.post('/seller/login',{
                email:document.getElementById('email').value,
                password:document.getElementById('password').value,
                remember:document.getElementById('remember').checked,
            })
            .then(function (response) {
                // handle success
                console.log(response);
                toastr.success(response.data.message);
                window.location.href = "/seller/";
            })
            .catch(function (error) {
                // handle error
                console.log(error);
                toastr.error(error.response.data.message);
            })
            .then(function () {
                // always executed
            });
        }

    </script>

  </body>
</html>
