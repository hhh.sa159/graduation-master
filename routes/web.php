<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\SellerAuthController;
use App\Http\Controllers\SellerController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::prefix('admin')->controller(App\Http\Controllers\Admin\HomeController::class)->middleware('auth:admins')->group(function () {

//     Route::get('/', 'home');
//     // Route::resource('admins', AdminController::class);
// });

// Route::prefix('seller')->controller(App\Http\Controllers\Seller\HomeController::class)->middleware('auth:sellers')->group(function () {

//     Route::get('/', 'home');
//     // Route::resource('categories', CategoryController::class);
//     // Route::resource('admins',AdminController::class);
//     // Route::post('logout', [AuthController::class, 'logout'])->name('logout');
// });

// Route::controller(App\Http\Controllers\User\HomeController::class)->group(function () {

//     Route::get('/', 'home');
// });

// Route::prefix('admin')->controller(App\Http\Controllers\Admin\AuthController::class)->group(function () {
//     Route::get('login', 'showLogin')->name('admin.login');
//     Route::post('login', 'login');
// });

// Route::prefix('seller')->controller(App\Http\Controllers\Seller\AuthController::class)->group(function () {
//     Route::get('login', 'showLogin')->name('seller.login');
//     Route::post('login', 'login');
// });

// Route::controller(App\Http\Controllers\User\AuthController::class)->group(function () {
//     Route::get('login', 'showLogin')->name('login');
//     Route::post('login', 'login');
// });

Route::prefix('admin')->middleware('guest:admin')->group(function(){

    // Route::view('login','cms.login')->name('login');
    Route::get('login',[AuthController::class,'showLogin'])->name('login.admin');
    Route::post('login',[AuthController::class,'login']);

});

Route::prefix('admin')->middleware('auth:admin')->group(function(){

    Route::view('/','cmsAdmin.demo');

    Route::resource('users',UserController::class);
    Route::resource('sellers',SellerController::class);
    Route::resource('admins',AdminController::class);
    Route::get('logout',[AuthController::class,'logout'])->name('logout.admin');
});

Route::prefix('seller')->middleware('guest:seller')->group(function(){

    // Route::view('login','cms.login')->name('login');
    Route::get('login',[SellerAuthController::class,'showLogin'])->name('login.seller');
    Route::post('login',[SellerAuthController::class,'login']);

});

Route::prefix('seller')->middleware('auth:seller')->group(function(){

    Route::view('/','seller.home');

    // Route::resource('cities',CityController::class);
    Route::resource('categories',CategoryController::class);
    Route::get('logout',[SellerAuthController::class,'logout'])->name('logout.seller');
});
